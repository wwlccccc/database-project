// pages/tabBar/cinema/cinema.js
const util = require('../../../utils/util.js')
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    city: '正在定位...', // 城市参数
    title: '', // ？
    params: { // 初始化页面加载的参数对象
      pageNum: 0,
      limit: 8,
      hallType: -1,
      brandId: -1,
      serviceId: -1,
      selectRegion: ''
    },
    hasMore: true, // 是否还有更多的
    cinemas: [], //影院列表
    cityCinemaInfo: {}, //城市影院信息
    isShow: false, //导航下拉框是否展开
    nothing: false, //是否有符合过滤的影院
    noSchedule: false //当天是否有场次，原本时间是由后台返回的，但是缺少城市ID就没有返回，导致当天可能没有播放场次
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    var that = this
    that.cityInit();
    that.getFilter()
    that.getCinemas(that.data.params);
  },
  // 过滤城市的操作
  getFilter() {
    var that = this;
    wx.request({
      url: app.globalData.url + '/cinema/getFilters',
      method: 'POST',
      data: app.globalData.selectCity,
      success: function (res) {
        console.log(res.data)
        that.setData({
          cityCinemaInfo: res.data
        })
      }
    })
  },
  // 获取电影院的相关操作
  getCinemas(params) {
    const _this = this;
    var pageIndex = _this.data.params.pageNum;
    //  发送请求
    return new Promise((resolve, reject) => {
      wx.request({
        url: app.globalData.url +`/cinema/getCinemas`,
        method: 'POST',
        header: {
          'content-type': 'application/x-www-form-urlencoded'
        },
        data: {
          ...params,
          pageNum: ++pageIndex,
          selectCity: JSON.stringify(app.globalData.selectCity)
        },
        success(res) {
          wx.hideLoading()
          // 成功就返回数据
          resolve(res.data.data.beanList)
          if (!res.data.data.beanList.length) {
            _this.setData({
              noSchedule: true,
              hasMore: false
            })
            return
          }
          // 更新数据值
          _this.setData({
            hasMore: pageIndex < res.data.data.tr,
            cinemas: _this.data.cinemas.concat(res.data.data.beanList),
            params: {
              ..._this.data.params,
              pageNum: pageIndex
            }
          })
        }
      })
    })
    
  },
  // 城市的初始化操作
  cityInit: function () {
    if (app.globalData.userLocation) {
      this.setData({
        city: app.globalData.selectCity ? app.globalData.selectCity.cityName : '定位失败'
      })
    } else {
      app.userLocationReadyCallback = () => {
        // 在location回调之后再次判断
        this.setData({
          city: app.globalData.selectCity ? app.globalData.selectCity.cityName : '定位失败'
        })
      }
    }
  },
  // 过滤器条件改变的渲染情况
  changeCondition(e) {
    const obj = e.detail
    wx.showLoading({
      title: '正在加载...'
    })
    this.setData({
      params: {
        ...this.data.params,
        ...obj,
        pageNum: 0
      },
      cinemas: [],
      nothing: false
    }, () => {
      // 获取筛选的影院列表
      this.getCinemas(this.data.params).then((list) => {
        if (!list.length) {
          this.setData({
            nothing: true
          })
        }
        wx.hideLoading()
      })
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    wx.setNavigationBarTitle({
      title: '电影院'
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // 城市更新之后再次获取
    if (app.globalData.selectCity) {
      this.setData({
        city: app.globalData.selectCity.cityName
      })
    }
    // 返回当前页面的时候是否刷新了
    if (app.globalData.isRefresh) {
      app.globalData.isRefresh = false;
      this.setData({
        params: {
          ...this.data.params,
          pageNum: 0
        },
        city: app.globalData.selectCity.cityName,
        cinemas: [],
        nothing: false
      })
      this.getFilter()
      this.getCinemas(this.data.params)
    }
  },
  //导航下拉框状态变化时的处理
  toggleShow(e) {
    const item = e.detail.item
    this.setData({
      isShow: item !== -1
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数,加载更多
   */
  onReachBottom() {
    if (!this.data.hasMore) return
    this.getCinemas(this.data.params)
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})