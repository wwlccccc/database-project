// pages/tabBar/user/user.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    avatarUrl:'../../../static/images/avatar.png', // 用户未登录时默认头像存放位置
    hasUserInfo:false,  // 是否用户已经登录，含有用户信息
    username:'点击登录', // 用户名称
  },

  onShareAppMessage(){
    return {
      title: 'iLove-电影',
      path: 'pages/movie/movie'
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 每次加载寻找用户是否已授权
    this.userAuthorized();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.userAuthorized();
  },

   // 用户授权方法实现
  userAuthorized(){
    var _this = this;
    // 首先看缓存是否有
    const userInfo = wx.getStorageSync('userInfo');
    if(userInfo){
      //存在则判断服务端session是否过期
      // console.log("判断服务端session是否过期")
      // 发送request请求
      wx.request({
        url: app.globalData.url+'/user/isAuth',
        method:'GET',
        header:{
          'token':userInfo.data.token
        },
        // 成功返回之后
        success(res){
          if(_this.data.state == 202){ // 认证失败
            _this.setData({
              hasUserInfo:false
            })
          }else{
            // console.log("认证成功");
            _this.setData({  // 认证成功
              hasUserInfo:true
            })
          }
        }
      })
    }else{
      _this.setData({  // 本地也没有缓存的
        hasUserInfo:false
      })
    }
  },
  // 在没有本地缓存之后，我们需要用户进行登录操作，与open-type的getUserInfo相对应
  // 会有事件响应
  onGetUserInfo(event){
    var that = this;
    const userInfo = event.detail.userInfo
    // console.log(userInfo,"yyy")
    if(userInfo){
      wx.login({
        success:function(resp){
          wx.getUserInfo({
            success:function(resp1){
              // console.log("resp:",resp)
              wx.request({
                url: app.globalData.url+'/user/wxLogin',
                method:'POST',
                header:{
                  'content-type': 'application/x-www-form-urlencoded'
                },
                data:{
                  code:resp.code,
                  nickName:resp1.userInfo.nickName,
                  avatarUrl:resp1.userInfo.avatarUrl,
                  gender:resp1.userInfo.gender
                },
                success:function(res){
                  const userInfo = res.data
                  wx.setStorage({
                    key:'userInfo',
                    data: userInfo
                  })
                  that.setData({
                    hasUserInfo:true
                  })
                  app.globalData.userInfo = resp1.userInfo
                }
              })
            }
          })
        }
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})