// pages/tabBar/movie/movie.js
// 获取应用的实例
const app = getApp();
const util = require('../../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    city: '正在定位...', // 城市定位及其选择
    itemsShow: [], // 正在热映的电影集合
    itemsWait: [], // 即将上映的电影集合
    currentTab: 0, // 当前选择的页面 0为正在热映，1为即将上映
    swiperViewHeight: 0, // swiper主页的高度 视图
    // swiper-item属性 主要是指轮播图
    indicatorDots: true, // 是否显示面板指示点
    vertical: false, // 是否显示面板指示点
    autoplay: true, // vertical: false,
    interval: 3000, // 自动切换时间间隔
    duration: 1200, // 滑动动画时长
    // 轮播图列表集合
    bannerList: [],
    // 即将上映的列表集合
    itemsPop: [],
    // 正在热映是否还有更多
    hasMoreHost: true,
    // 即将上映是否还有更多
    hasMoreWait: true,
    pageNumHost: 0, // 正在放映的加载的数据页面数量
    pageNumWait: 0, // 即将放映的加载的数据页面数量
    limit: 6, // 每个页面显示的数量
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    // 加载页面数据
    var that = this;
    that.loadBannerList();
    // city定位初始化
    that.cityInit();
    // 获取正在热映的电影列表
    that.getMovieList();
    that.getMovieWait();
    // 获取受到欢迎的电影列表
    that.getMoviePop();
  },

  // 获取受到欢迎的电影列表
  getMoviePop() {
    var that = this;
    wx.request({
      url: app.globalData.url + '/home/getMoviePop',
      method: 'GET',
      data: {},
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
        // console.log("popmovie=", res)
        var list = res.data.data;
        for (var i in list) {
          // 转换时间格式
          list[i].rt = util.formatDate(new Date(list[i].rt));
        }
        that.setData({
          itemsPop: list
        })
      }
    })
  },


  // 获取正在热映的电影列表
  async getMovieList() {
    var that = this;
    var pageNumHost = that.data.pageNumHost;
    var limit = that.data.limit;
    // 请求获取方法
    await wx.request({
      url: app.globalData.url + '/home/getMovieList',
      method: 'GET',
      data: {
        pageNum: ++pageNumHost,
        limit: limit
      },
      header: {
        'Accept': 'application/json'
      },
      success: function (res) {
        const itemsShow = that.data.itemsShow.concat(res.data.data.beanList);
        // console.log("resp", res.data.data.beanList)
        that.setData({
          hasMoreHost: pageNumHost < res.data.data.tr,
          itemsShow: itemsShow,
          pageNumHost: pageNumHost
        })
        if (that.data.currentTab == 0) {
          that.setSwiperHeight();
        }
      }
    })
  },

  // 获取即将上映的电影数据
  getMovieWait: function () {
    var that = this;
    var pageNumWait = that.data.pageNumWait;
    var limit = that.data.limit;
    wx.request({
      url: app.globalData.url + '/home/getMovieWait',
      method: 'GET',
      data: {
        pageNum: ++pageNumWait,
        limit: limit
      },
      success: function (res) {
        const itemsWait = that.data.itemsWait.concat(res.data.data.beanList);
        that.setData({
          hasMoreWait: pageNumWait < res.data.data.tr,
          itemsWait: itemsWait,
          pageNumWait: pageNumWait
        })
        if (that.data.currentTab == 1)
          that.setSwiperHeight();
      }
    })
  },

  // 当滑动到底部继续加载时
  

  // 加载轮播图的方法
  loadBannerList: function () {
    var that = this;
    // 获取全局数据  调用应用实例的方法获取全局数据 bannerList
    wx.request({
      url: app.globalData.url + '/home/getBannerList',
      method: 'GET',
      data: {},
      success: function (res) {
        that.setData({
          bannerList: res.data.data
        })
        // console.log('bannerList=', that.bannerList)
      }
    })
  },

  // city初始化-定位功能
  cityInit: function () {
    if (app.globalData.userLocation) {
      this.setData({
        city: app.globalData.selectCity ? app.globalData.selectCity.cityName : '定位失败'
      })
    } else {
      app.userLocationReadyCallback = () => {
        this.setData({
          city: app.globalData.selectCity ? app.globalData.selectCity.cityName : '定位失败'
        })
      }
    }
  },
  //滑动切换页面的具体方法
  swiperTab: function (e) {
    var that = this;
    that.setSwiperHeight();
    wx.pageScrollTo({
      scrollTop: 0
    })
    that.setData({
      currentTab: e.detail.current
    });
  },

  //点击切换页面
  clickTab: function (e) {
    var that = this;
    if (this.data.currentTab === e.target.dataset.current) {
      return false;
    } else {
      that.setSwiperHeight();
      that.setData({
        currentTab: e.target.dataset.current
      })
    }
  },
  // 设置tab页面的高度
  setSwiperHeight: function () {
    var query = wx.createSelectorQuery();
    //选择id
    var that = this;
    var px1 = 208 / 750 * wx.getSystemInfoSync().windowWidth;
    var px2 = 376 / 750 * wx.getSystemInfoSync().windowWidth;
    query.selectAll('.swiperH').boundingClientRect(function (rect) {
      // console.log(rect)
      var itemsLength = 0;
      if (that.data.currentTab == 0) {
        itemsLength = rect[0].height * that.data.itemsShow.length + px1;
      } else if (that.data.currentTab == 1) {
        itemsLength = rect[0].height * that.data.itemsWait.length + px2;
      }
      that.setData({
        swiperViewHeight: itemsLength
      })
    }).exec();
  },

  swiperchange: function (e) {
    //FIXME: 当前页码
    //console.log(e.detail.current)
    // 凑数的没具体实现
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    // 返回此页面时，设置选择的city
    if (app.globalData.selectCity) {
      this.setData({
        city: app.globalData.selectCity.cityName
      })
    }

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.currentTab == 0 && this.data.hasMoreHost) {
      // console.log(this.data.hasMoreHost)
      this.getMovieList();
    } else if (this.data.currentTab == 1 && this.data.hasMoreWait) {
      this.getMovieWait();
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})