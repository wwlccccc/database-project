// pages/subPages/about-page/about-page.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info:[
      {
        label:'姓名',
        value:'wwl'
      },
      {
        label: '职业',
        value: 'xuesheng'
      },
      {
        label: 'QQ',
        value: '2215058009'
      },
      {
        label: '微信',
        value: 'xx2215058009'
      },
      {
        label: '项目',
        value: '数据库系统课程设计'
      }
    ]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})