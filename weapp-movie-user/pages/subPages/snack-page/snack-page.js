// pages/subPages/snack-page/snack-page.js
const util = require('../../../utils/util.js');
const getRandom = util.getRandom;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    info: null, //小吃详情
    cinemaName: '',
    cinemaData:null, //影院地图详情
    cinemaId: null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    console.log(options)
    const paramsObj = JSON.parse(decodeURIComponent(options.paramsStr))
    this.initPage(paramsObj)
  },

  initPage(obj, param) {
    wx.showLoading({
      title: '正在加载',
    })
    this.setData({
      cinemaName: obj.cinemaName,
      cinemaId: obj.cinemaId,
      cinemaData: obj.cinemaData,
      info: obj.info
    })
    wx.hideLoading();
  },

  //跳转到“提交订单”页面
  buySnack(){
    const { info, cinemaName, cinemaId} = this.data
    //添加订单信息
    const paramsStr = JSON.stringify({
      cinemaName,
      cinemaId,
      snackId: info.id,
      title: info.firstTitle,//套餐名
      orderId: getRandom(1000000000, 9999999999),
      img: info.imageUrl,//图片
      amount:1,//数量
      price: info.price,//单价
      total: info.price * 1//合计
    })
    wx.navigateTo({
      url: `/pages/subPages/buy-snack/buy-snack?paramsStr=${encodeURIComponent(paramsStr)}`,
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})