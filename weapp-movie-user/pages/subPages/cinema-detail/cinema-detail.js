// pages/subPages/cinema-detail/cinema-detail.js
const util = require('../../../utils/util.js');
const formatNumber = util.formatNumber;
const getRandom = util.getRandom;
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    cinemaId: '',
    movieId: '',
    cinemaDetail: null, //影院详情
    movie: null, //选中的电影
    movies: null, //电影列表
    days: [], //该电影的排片日期列表
    timeList: [], //当天播放电影的时间段
    divideDealList: [], //影院分类零食列表
    first: true //只在第一次提示
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.initPage(options);
  },

  //初始化页面
  initPage(query) {
    // 构造查询条件
    // 加载初始化的相关数据，即是当前影院的数据
    const {
      cinemaId = '', movieId = '', day = ''
    } = query
    this.setData({
      cinemaId,
      movieId,
      day
    })
    const _this = this
    wx.showLoading({
      title: '正在加载...',
    })
    wx.request({
      url: app.globalData.url + `/cinema/getCinema/${cinemaId}`,
      method: 'GET',
      data: {
        movieId: _this.data.movieId
      },
      success(res) {
        wx.hideLoading()
        _this.setData({
          cinemaDetail: res.data.data.cinema,
          movies: res.data.data.movies,
          divideDealList: res.data.data.snacks
        })
      }
    })
  },

  //选择电影
  selectMovie(e) {
    const movie = e.detail.movie;
    console.log(movie)
    let days = [];
    movie.movieDays.forEach(item => {
      days.push({
        title: util.formatDate(new Date(item.day)),
        day: item.day
      })
    })
    this.setData({
      movie,
      days,
      timeList: this.createEndTime(movie.movieDays[0].times, movie.dur, movie.version)
    })
  },

  //选择时间,观看时间
  selectDay(e) {
    const day = e.detail.day
    const movie = this.data.movie
    const index = movie.movieDays.findIndex(item => item.day === day)
    console.log(movie.movieDays)
    this.setData({
      timeList: this.createEndTime(movie.movieDays[index].times, movie.dur, movie.version)
    })
  },
  //购票，跳转到购票页面
  buyTicket(e){
    const info = JSON.stringify(e.currentTarget.dataset.info);
    console.log(info)
    const movie = JSON.stringify(this.data.movie);
    const cinema = JSON.stringify(this.data.cinemaDetail);
    // console.log("info",info);
    // console.log("info",movie);
    // console.log("info",cinema);
    wx.navigateTo({
      url: '/pages/subPages/seat-select/seat-select?movie='+encodeURIComponent(movie)+'&info='+encodeURIComponent(info)+'&cinema='+encodeURIComponent(cinema),
    })
  },
  //跳转到“套餐详情”页面
  goSnackPage(e){
    const info = e.currentTarget.dataset.info;
    //将参数转化为JSON通过页面跳转时传递
    const paramsStr = JSON.stringify({
      cinemaName: this.data.cinemaDetail.nm,
      cinemaId: this.data.cinemaId,
      cinemaData: this.data.cinemaDetail,//影院信息
      info: info
    })
    wx.navigateTo({
      url: `/pages/subPages/snack-page/snack-page?paramsStr=${encodeURIComponent(paramsStr)}`,
    })
  },
  //处理散场时间,计算出散场时间并显示
  createEndTime(arr, dur,version) {
    let timeList = []
    if (Array.isArray(arr)) {
      timeList = arr.map(item => {
        let temp = { ...item
        }
        temp.version = version && version.split(' ').map(item => {
          return item.toUpperCase().replace('V', '')
        })
        console.log(item.startTime);
        var time = new Date(item.startTime)
        console.log(time);
        temp.beginTime = `${formatNumber(time.getHours())}:${formatNumber(time.getMinutes())}`
        time = time.setMinutes(time.getMinutes() + dur)
        const endTime = new Date(time)
        temp.endTime = `${formatNumber(endTime.getHours())}:${formatNumber(endTime.getMinutes())}`
        return temp;
      })
    }
    return timeList;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})