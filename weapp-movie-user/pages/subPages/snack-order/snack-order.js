// pages/subPages/snack-order/snack-order.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderList: [], // 订单列表
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.initData();
  },

  initData() {
    var that = this;
    wx.getStorage({
      key: 'userInfo',
      success: function (res1) {
        wx.request({
          url: app.globalData.url + '/order/getSnackOrder',
          method: "GET",
          header: {
            'token': res1.data.data.token
          },
          success(res) {
            if(res.data.state == 202){
              wx.showModal({
                content: '您还未登录',
                success(res) {
                  wx.switchTab({
                    url: '/pages/subBar/user/user',
                  })
                }
              })
            }else{
              that.setData({
                orderList: res.data.data
              })
            }
          }
        })
      }
    })
  },
  //删除订单
  deleteOrder(e) {
    const index = e.currentTarget.dataset.index;
    let orderList = this.data.orderList.slice();
    orderList.splice(index, 1)
    wx.showModal({
      title: '提示',
      content: '确认删除订单吗？',
      success: (res) => {
        if (res.confirm) {
          this.setData({
            orderList
          })
          wx.setStorageSync('snackOrder', orderList)
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})