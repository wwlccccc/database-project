// pages/subPages/movie-order/movie-order.js
const app = getApp();
const util = require("../../../utils/util.js")
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderList:[], // 订单列表
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {
    this.initData();
  },
  initData(){
    var that = this;
    wx.getStorage({
      key: 'userInfo',
      success: function(res1) {
        wx.request({
          // 拿到本地缓存，获取该客户的订单信息
          url: app.globalData.url +'/order/getMovieOrder',
          method: "GET",
          header:{
            'token': res1.data.data.token
          },
          success(res){
            console.log(res);
            if(res.data.state === 202){
              wx.showModal({
                content: '您还未登录',
                success(res) {
                  wx.switchTab({
                    url: '/pages/tabBar/user/user',
                  })
                }
              })
            }else{
              res.data.data.forEach(order => { order.time = util.formatYear(new Date(order.time))})
              that.setData({
                orderList: res.data.data
              })
            }
          }
        })
      }
    })
  },

  //跳转到订单详情页面
  goTo(e){
    const order = e.currentTarget.dataset.order;
    order.Vcode = util.getRandom(100000, 999999);
    const paramsStr = JSON.stringify(order);
    wx.navigateTo({
      url: '../movie-order-detail/movie-order-detail?paramsStr='+encodeURIComponent(paramsStr),
    })
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})