// pages/subPages/search-page/search-page.js
const app = getApp();
const util = require('../../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    value: '', // 搜索框的输入值
    stype: '', // 选择搜索类型，//搜索类型，-1代表搜索影院或电影，2代表搜索影院
    placeholder: '', // 搜索框的占位符
    movies: {}, // 电影集合
    cinemas: {}, //影院集合
    history: [], //历史搜索记录，本地缓存了
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    this.initPage(options)
    this.setData({
      history: wx.getStorageSync("history") || []
    })
  },
  // 页面初始化 选定搜索类型
  initPage(query) {
    //搜索类型，-1代表搜索影院或电影，2代表搜索影院
    const stype = query.stype
    let placeholder = ''
    if (stype === '-1') {
      placeholder = '搜电影、搜影院'
    } else {
      placeholder = '搜影院'
    }
    this.setData({
      stype,
      placeholder
    })
  },

  // 搜索方法
  search(e) {
    var value = e.detail.value
    const _this = this
    console.log(e.target.dataset.index) // 打印navigation渲染值，要是在history中，直接从history拿到值
    if (e.target.dataset.index != null) {
      value = _this.data.history[e.target.dataset.index]
    } else {
      // 写入查询的历史记录中
      let history = wx.getStorageSync("history") || [];
      history.push(value)
      wx.setStorageSync("history", history);
    }
    // 设置值
    this.setData({
      value,
      history: []
    })
    // 现在发送请求方法
    wx.request({
      url: app.globalData.url + '/home/search',
      method: 'POST',
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      data: {
        keyword: value,
        stype: _this.data.stype,
        selectCity: JSON.stringify(app.globalData.selectCity)
      },
      success(res) {
        console.log(res)
        for (var index in res.data.data.movies) {
          // 时间格式化
          res.data.data.movies[index].rt = util.formatYear(new Date(res.data.data.movies[index].rt))
        }
        _this.setData({
          // 设置数据，判断返回的数据是否有效
          movies: res.data.data.movies ? res.data.data.movies : [],
          cinemas: res.data.data.cinemaVos ? res.data.data.cinemaVos : []
        })
      }
    })
  },
  // 返回上一级页面
  goBack() {
    wx.navigateBack({
      delta: 1
    })
  },

  //清除缓存历史，清除搜索记录
  clearHistory: function () {
    this.setData({
      history: []
    })
    wx.setStorageSync("history", [])
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})