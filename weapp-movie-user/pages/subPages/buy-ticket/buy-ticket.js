// pages/subPages/buy-ticket/buy-ticket.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    order: null, // 订单详细信息
    first: true //是否是第一次支付
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad(options) {
    const option = JSON.parse(decodeURIComponent(options.paramsStr));
    this.initData(option);
  },
  // 初始化数据
  initData(params) {
    this.setData({
      order: params
    })
  },

  //模拟支付,这里不再调用真实接口
  payment() {
    //避免重复支付
    if (this.data.first) {
      var that = this;
      // let movieOrder = wx.getStorageSync('movieOrder') || []
      // movieOrder.unshift(this.data.order)
      // wx.setStorageSync('movieOrder', movieOrder)
      //请求后台模拟支付
      const order = this.data.order;
      wx.getStorage({
        key: 'userInfo',
        success: function (res1) {
          // 提交订单请求
          wx.request({
            url: app.globalData.url + '/order/addOrder',
            method: 'POST',
            header: {
              'content-type': 'application/x-www-form-urlencoded',
              'token': res1.data.data.token
            },
            data: {
              itemId: order.movieId,
              itemType: '电影票',
              relateId: order.timesId,
              orderId: order.orderId,
              price: order.price,
              seat: JSON.stringify(order.seat)
            },
            success(res) {
              if (res.data.state == 202) {
                wx.showModal({
                  content: '您还未登录',
                  success(res) {
                    wx.switchTab({
                      url: '/pages/tabBar/user/user',
                    })
                  }
                })
              } else if (res.data.state === 200) {
                wx.showToast({
                  title: '支付成功',
                  icon: 'success'
                })
                that.setData({
                  first: false
                })
                // wx.navigateTo({
                //   url: `../movie-order-detail/movie-order-detail?paramsStr=${JSON.stringify(res.data.data)}`,
                // })
                wx.switchTab({
                  url: '/pages/tabBar/user/user',
                })
              } else {
                wx.showToast({
                  title: '支付失败',
                  icon: 'error'
                })
              }
            }
          })
        },
        fail: function (res2) {
          wx.showModal({
            content: '您还未登录',
            success(res) {
              wx.switchTab({
                url: '/pages/tabBar/user/user',
              })
            }
          })
        }
      })
    } else {
      wx.showToast({
        title: '已支付',
        icon: 'none'
      })
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage() {

  }
})