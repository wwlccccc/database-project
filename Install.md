## Install

### 项目配置教程

下载IDEA：https://www.jetbrains.com/idea/

下载fastGithub：https://cloud.tsinghua.edu.cn/d/df482a15afb64dfeaff8/

JAVA配置

[(303条消息) JDK1.8下载与安装（完整图文教程含安装包）_jdk1.8安装包_猿月亮的博客-CSDN博客](https://blog.csdn.net/VA_AV/article/details/125602118)



MAVEN配置

[(303条消息) Maven安装及环境配置（详细教程）_122956985_response_L的博客-CSDN博客](https://blog.csdn.net/qq_44306545/article/details/122956985)



NODE配置

[(303条消息) Node.js安装与配置（详细步骤）_nodejs安装及环境配置_liyitongxue的博客-CSDN博客](https://blog.csdn.net/qq_42006801/article/details/124830995)



REDIS配置

下载：https://github.com/tporadowski/redis/releases

[(303条消息) Redis安装配置教程（Linux+Windows）_头顶一点云的博客-CSDN博客](https://blog.csdn.net/HBliucheng/article/details/112467680)



项目pkg

激活IDEA

IDEA导入项目（分为前端和后端）

前端执行npm install npm run dev(要是权限请寻求超级管理员的帮助)

配置config/index.js第15行，24-25行

![image-20230309205442837](./images/image-20230309205442837.png)

出现错误如下：（考虑网络连接问题）

[npm install 报错 Error: connect ETIMEDOUT 20.205.243.166:443_npm_好牛的码客-DevPress官方社区 (csdn.net)](https://huaweicloud.csdn.net/638f1421dacf622b8df8ee37.html?spm=1001.2101.3001.6650.2&utm_medium=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~activity-2-121678966-blog-107788134.pc_relevant_3mothn_strategy_and_data_recovery&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~activity-2-121678966-blog-107788134.pc_relevant_3mothn_strategy_and_data_recovery&utm_relevant_index=3)

[(303条消息) npm connect ETIMEDOUT_旅行者yky的博客-CSDN博客](https://blog.csdn.net/y_k_y/article/details/86534994?spm=1001.2101.3001.6650.5&utm_medium=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~Rate-5-86534994-blog-107788134.pc_relevant_3mothn_strategy_and_data_recovery&depth_1-utm_source=distribute.pc_relevant.none-task-blog-2~default~CTRLIST~Rate-5-86534994-blog-107788134.pc_relevant_3mothn_strategy_and_data_recovery&utm_relevant_index=6)

[(303条消息) failed, reason: connect ETIMEDOUT 104.16.20.35:443、 Could not install from “node_modules\vue-fancy_搬砖工人1024的博客-CSDN博客](https://blog.csdn.net/qq_25378657/article/details/107788134)

![image-20230309202715011](./images/image-20230309202715011.png)

经查明：纯属网原因，重新执行一遍就ok，可能与开代理也有关系，建议关闭代理，重启计算机，然后就基本上ok了

后端出现错误如下：

![image-20230309204039196](./images/image-20230309204039196.png)

![image-20230309204113912](./images/image-20230309204113912.png)

改完之后重新刷新maven就可以了



MYSQL连接错误

![image-20230309205837028](./images/image-20230309205837028.png)

[(303条消息) 连接mysql时报错Public Key Retrieval is not allowed的解决方法_往事不堪回首..的博客-CSDN博客](https://blog.csdn.net/qq3892997/article/details/122570788)



OSS客户端未注册错误

![image-20230309210140717](./images/image-20230309210140717.png)



配置相关文件：文件位于util/constant下面

注意：申请腾讯地图和百度地图api的时候需要注意申请小程序app/web服务，需要用到appkey  and appsecret；这部分参数来自与微信公众平台：

相关参数已申请配置即可（记得配置申请就ok）



微信小程序端：

1. 修改app.js中的qqmapsdk:
2. url链接：![image-20230309204744171](./images/image-20230309204744171.png)
3. app.json文件中加入：作用域，小程序官方要求
![image-20230309204902198](./images/image-20230309204902198.png)

4. 这个 头像是加载不出来的，这个路径建议修改：

   ![image-20230309211056291](./images/image-20230309211056291.png)

5. 微信公众平台的配置细节

   ![image-20230309211505189](./images/image-20230309211505189.png)

**appkey和密钥**

api接口调用请求地址

包括：地图的api和阿里云oss的api地址调用

阿里云请求的四个参数记得去申请就ok

![image-20230309211655639](./images/image-20230309211655639.png)





### 项目部署

项目部署这边只考虑web前端和web后端，暂时不考虑微信小程序端，这部分暂时忽略。

[(303条消息) 使用Dockerfile 构建一个Maven项目_dockerfile 编译maven_胸大的请先讲的博客-CSDN博客](https://blog.csdn.net/qq_42428264/article/details/110943347)

1. 服务端打包

   首先修改redis的配置路径，这部分建议将redis部署在云服务器上（这里是直接用一台azure的服务器进行演示相关操作）

2. 先去azure开一台ubuntu服务器环境

   我们现在连接上服务器

   ```
   ssh username@ip -p 22
   enter your password
   ```

3. [Install Docker Engine on Ubuntu](https://docs.docker.com/engine/install/ubuntu/) 参考官方文件安装

   ![image-20230309230116928](./images/image-20230309230116928.png)

   基本上到这里就ok了

4. ```
   sudo docker ps -a
   sudo docker images // 检查是否安装成功
   ```

5. 安装redis

   [最详细的docker中安装并配置redis - 腾讯云开发者社区-腾讯云 (tencent.com)](https://cloud.tencent.com/developer/article/1670205)

   ```
   sudo docker pull redis
   sudo mkdir -p /data/redis
   sudo mkdir -p /data/redis/data
   sudo cp -p redis.conf /data/redis/
   ```

   ```
   sudo docker run -p 6379:6379 --name redis -v /data/redis/redis.conf:/etc/redis/redis.conf  -v /data/redis/data:/data -d redis redis-server /etc/redis/redis.conf --appendonly yes
   ```

   然后需要去azure官网开放6379端口，**同时开启ubuntu主机的防火墙（可选项）**（千万别开ubuntu主机的防火墙，请参考：[(303条消息) 服务器开启防火墙导致无法ssh链接_ssh连接面板输入ufw disable_TUANZI_Dum的博客-CSDN博客](https://blog.csdn.net/qq_23066945/article/details/103111912)）

   被锁定解决方案：

   ![image-20230310003952813](./images/image-20230310003952813.png)

   [azure linux虚拟机开放端口 - pinkie - 博客园 (cnblogs.com)](https://www.cnblogs.com/pinkie/p/14002696.html)

   [(303条消息) Ubuntu系统中防火墙的使用和开放端口_ubuntu firewall_Aaron_Run的博客-CSDN博客](https://blog.csdn.net/qq_36938617/article/details/95234909)

6. 安装mysql（暂时略）

7. package打包 执行mvn package

   ![image-20230310000623866](./images/image-20230310000623866.png)
   ![image-20230310000854618](./images/image-20230310000854618.png)

    打包之后的目录如上图所示：

8. 安装xftp：[XFTP - NetSarang Website (xshell.com)](https://www.xshell.com/zh/xftp/)

9. 连接服务器（sftp，可以直接通过 xshell连接，上传刚才打包好的文件夹）

   ![image-20230310004413005](./images/image-20230310004413005.png)

10. 执行dockerfile和.sh文件

    ```dockerfile
    # 指定openjdk8作为镜像基础
    FROM openjdk:8
    # 作者信息
    MAINTAINER xxxxxxxxxx@qq.com xxxxxxxxxx
    # Dockerfile文件所在目录下的install-maven.sh 复制到 镜像的/root 目录下
    ADD ./install-maven.sh /root
    # 将spring boot打好可执行文件复制到镜像/root目录下
    ADD ./db-project-0.0.1-SNAPSHOT.jar /root
    # 容器中使用到的8000端口号
    EXPOSE 8987
    # 容器启动时执行`java -jar xxx.jar`命令
    ENTRYPOINT java -jar /root/db-project-0.0.1-SNAPSHOT.jar
    ```

    ```shell
    #!/bin/bash
    wget -P /usr/local/ https://archive.apache.org/dist/maven/maven-3/3.8.4/binaries/apache-maven-3.8.4-bin.tar.gz
    tar -xvf /usr/local/apache-maven-3.8.4-bin.tar.gz -C /usr/local
    echo "export MAVEN_HOME=/usr/local/apache-maven-3.8.4" >> /etc/profile
    echo "export PATH=\$PATH:\$MAVEN_HOME/bin" >> /etc/profile
    source /etc/profile
    mvn -version
    ```

    在当前目录下执行

    ```
    sudo docker build -t dbproject .
    sudo docker images // 查看创建的镜像
    sudo docker run -it --name dbproject -p 8987:8987 dbproject /bin/bash
    ```

    记得去防火墙开放8987端口

    服务器重启之后别忘了docker中的进程也重启一下

    我们通过修改小程序的访问url（位于app.js文件中）可以测试后台访问进程是否正常。

### 前端打包

1. 修改config/index.js中的文件如下，主要是后端访问url的修改：

   ![image-20230310010239169](./images/image-20230310010239169.png)

2. 执行build：打包：

   ![image-20230310010343774](./images/image-20230310010343774.png)

3. 打包之后在dist文件下下面：

   ![image-20230310010750793](./images/image-20230310010750793.png)

4. 对dist文件夹打包得到static.zip 上传到云服务器

5. 安装nginx

   ```shell
   sudo apt-get install nginx
   ```

6. 修改nginx的配置文件

   ```nginx
   user  nginx;
   worker_processes  auto;
   
   error_log  /var/log/nginx/error.log notice;
   pid        /var/run/nginx.pid;
   
   
   events {
       worker_connections  1024;
   }
   
   
   http {
       include       /etc/nginx/mime.types;
       default_type  application/octet-stream;
   
       server {
           listen 80;
           server_name localhost; // 这个要与前端config/index.js的配置文件一致 这里不写分号死了一片
   
           location / {
           		proxy_pass  http://backend ip :8987;
           	}
           }
   
       log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                         '$status $body_bytes_sent "$http_referer" '
                         '"$http_user_agent" "$http_x_forwarded_for"';
   
       access_log  /var/log/nginx/access.log  main;
   
       sendfile        on;
       #tcp_nopush     on;
   
       keepalive_timeout  65;
   
       #gzip  on;
   
       include /etc/nginx/conf.d/*.conf;
   }
   
   ```

   上传该配置文件，

   ```
   nginx -t // 查看默认配置文件所在位置
   sudo cp ./nginx.conf /etc/nginx/nginx.conf
   cat /ect/nginx/nginx.conf // 查看是否修改
   // 解压上述压缩包到nginx默认访问目录
   sudo apt-get install zip
   unzip target.zip
   // 解压缩失策
   dbserver@dbubuntu:~$ sudo mv *.html /var/www/html/
   dbserver@dbubuntu:~$ sudo mkdir -p /var/www/html/static
   dbserver@dbubuntu:~$ sudo mv static/* /var/www/html/static/
   // 最后检查是否正确
   ```

   记得开启防火墙80端口

   ```
   // 重启nginx服务使得其生效
   useradd -M -s /sbin/nologin nginx
   cd /sbin
   sudo ./nginx -s reload 
   // 不知道需不需要下面一步骤，可能单纯加载慢死了
   sudo /etc/init.d/nginx start
   ```

   ![image-20230310012533240](./images/image-20230310012533240.png)

   [(303条消息) 启动nginx服务失败，nginx: [emerg\] getpwnam(“nginx“) failed_富士山fy的博客-CSDN博客](https://blog.csdn.net/m0_48760948/article/details/111567309)

   [(303条消息) 使用Nginx部署Vue项目超详细图文教程！含踩坑步骤！_nginx vue_Hi丶ImViper的博客-CSDN博客](https://blog.csdn.net/weixin_43314519/article/details/115151858)

   nginx最后是重启虚拟机试试搞好的（下次配环境仔细点，不要急）

   [(303条消息) Ubuntu系统的nginx启动_aaronthon的博客-CSDN博客](https://blog.csdn.net/aaronthon/article/details/84292654)


