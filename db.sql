
create table t_admin_menu
(
    id         int auto_increment
        primary key,
    permission varchar(255) null comment '具体权限'
)
    charset = utf8;

create table t_admin_role
(
    id   int auto_increment
        primary key,
    name varchar(255) not null comment '角色名'
)
    charset = utf8;

create table t_admin_role_menu
(
    id      int auto_increment
        primary key,
    role_id int not null,
    menu_id int not null,
    constraint t_admin_role_menu_ibfk_1
        foreign key (role_id) references t_admin_role (id),
    constraint t_admin_role_menu_ibfk_2
        foreign key (menu_id) references t_admin_menu (id)
)
    charset = utf8;

create index menu_id
    on t_admin_role_menu (menu_id);

create index role_id
    on t_admin_role_menu (role_id);

create table t_admin_user
(
    id        int auto_increment
        primary key,
    avatar    varchar(255) not null comment '头像',
    name      varchar(255) not null comment '昵称',
    username  varchar(255) not null comment '用户名',
    password  varchar(255) not null comment '密码',
    cinema_id int          null comment '相关影院'
)
    charset = utf8;

create table t_admin_user_role
(
    id            int auto_increment
        primary key,
    admin_user_id int not null,
    role_id       int not null,
    constraint t_admin_user_role_ibfk_1
        foreign key (admin_user_id) references t_admin_user (id),
    constraint t_admin_user_role_ibfk_2
        foreign key (role_id) references t_admin_role (id)
)
    charset = utf8;

create index admin_user_id
    on t_admin_user_role (admin_user_id);

create index role_id
    on t_admin_user_role (role_id);

create table t_banner
(
    id  int auto_increment
        primary key,
    img varchar(255) not null comment '轮播图',
    url varchar(255) null comment '跳转链接'
)
    charset = utf8;

create table t_cinema_brand
(
    id    int auto_increment
        primary key,
    brand varchar(255) not null comment '大地影院
星美国际影城
中影国际影城
万达影城
特色厅'
)
    charset = utf8;

create table t_cinema
(
    id          int auto_increment
        primary key,
    nm          varchar(255)   not null comment '电影院名',
    brand_id    int            null comment '所属品牌',
    addr        varchar(255)   null comment '电影院地址',
    latitude    decimal(65, 5) not null comment '纬度',
    longitude   decimal(65, 5) not null comment '经度',
    endorse     bit            not null comment '1支持退票',
    allowRefund bit            not null comment '1支持改签',
    constraint location
        unique (latitude, longitude),
    constraint t_cinema_ibfk_1
        foreign key (brand_id) references t_cinema_brand (id)
)
    charset = utf8;

create index brand_id
    on t_cinema (brand_id);

create fulltext index fullIndexCinema
    on t_cinema (nm, addr);

create table t_hall_type
(
    id       int auto_increment
        primary key,
    hallType varchar(255) not null comment '特色厅'
)
    charset = utf8;

create table t_hall
(
    id           int auto_increment
        primary key,
    cinema_id    int not null comment '所属影院',
    hall_type_id int null comment '厅的特色',
    constraint t_hall_ibfk_1
        foreign key (cinema_id) references t_cinema (id),
    constraint t_hall_ibfk_2
        foreign key (hall_type_id) references t_hall_type (id)
)
    charset = utf8;

create index cinema_id
    on t_hall (cinema_id);

create index hall_type_id
    on t_hall (hall_type_id);

create table t_movie
(
    id             int auto_increment
        primary key,
    nm             varchar(255)                not null comment '电影名',
    enm            varchar(255)                null comment '电影英文名',
    cat            varchar(255)                null comment '电影类型',
    dir            varchar(255)                not null comment '导演',
    img            varchar(500)                not null comment '宣传海报',
    version        varchar(255)                not null comment '播放类型（3d / imax）',
    dra            varchar(2000)               null comment '剧情描述',
    sc             double(255, 1) default 0.0  null comment '评分',
    snum           int            default 0    null comment '评分人数',
    star           varchar(255)                null comment '明星演员',
    src            varchar(255)                null comment '上映地区',
    language       varchar(255)                not null comment '语言',
    dur            int                         null comment '电影时长',
    globalReleased bit            default b'0' not null comment '是否上映过',
    wish           bigint         default 0    null comment '想看的人数',
    watched        int            default 0    null comment '观看人数',
    rt             date                        null comment '上映日期',
    showst         int            default 1    not null comment '状态：1想看 2预售 3上映 ',
    video_img      varchar(255)                null comment '预告视频图片',
    video_name     varchar(255)                null comment '预告视频的名字',
    video_url      varchar(255)                null comment '预告视频的url',
    photos         varchar(2000)               null comment '电影海报'
)
    charset = utf8;

create table t_days
(
    id        int auto_increment
        primary key,
    cinema_id int  not null comment '所属影院',
    movie_id  int  not null comment '所属电影',
    day       date not null comment '档期日期',
    constraint t_days_ibfk_1
        foreign key (cinema_id) references t_cinema (id),
    constraint t_days_ibfk_2
        foreign key (movie_id) references t_movie (id)
)
    charset = utf8;

create index CinemaAndMovieAndDay
    on t_days (cinema_id, movie_id, day);

create index movie_id
    on t_days (movie_id);

create fulltext index fullIndex
    on t_movie (nm, cat, dir, dra, star);

create table t_seat
(
    id      int auto_increment
        primary key,
    hall_id int          not null comment '所属的影厅id',
    y_coord int          not null comment '行',
    x_coord int          not null comment '列',
    status  varchar(255) not null comment '座位状态：有座 ok，无座booked',
    type    varchar(255) not null comment '座位类型：danren，road',
    constraint t_seat_ibfk_1
        foreign key (hall_id) references t_hall (id)
)
    charset = utf8;

create index hall_id
    on t_seat (hall_id);

create table t_snack
(
    id           int auto_increment
        primary key,
    cinema_id    int            not null comment '所属影院',
    image_url    varchar(255)   not null comment '显示图片',
    first_title  varchar(255)   not null comment '套餐名称',
    second_title varchar(255)   not null comment '详细描述',
    price        decimal(10, 2) not null comment '价格',
    cur_number   int            null comment '已售数目',
    total_number int            null comment '总数',
    constraint t_snack_ibfk_1
        foreign key (cinema_id) references t_cinema (id)
)
    charset = utf8;

create index cinema_id
    on t_snack (cinema_id);

create table t_times
(
    id         int auto_increment
        primary key,
    days_id    int            not null comment '所属档期',
    hall_id    int            not null comment '安排的厅号',
    start_time datetime       not null comment '开始时间',
    price      decimal(10, 1) not null comment '价格',
    constraint t_times_ibfk_1
        foreign key (days_id) references t_days (id),
    constraint t_times_ibfk_2
        foreign key (hall_id) references t_hall (id)
)
    charset = utf8;

create index days_id
    on t_times (days_id);

create index hall_id
    on t_times (hall_id);

create table t_user
(
    id         int auto_increment
        primary key,
    avatar_url varchar(255)     not null comment '头像url',
    nick_name  varchar(255)     not null comment '用户昵称',
    gender     varchar(255)     not null comment '性别',
    open_id    varchar(255)     not null comment 'open_id',
    last_login datetime         null comment '最后一次登录时间',
    is_banned  bit default b'0' not null comment '是否被禁用'
)
    charset = utf8;

create table t_comment
(
    id        int auto_increment
        primary key,
    user_id   int           not null comment '用户id',
    movie_id  int           not null comment '电影id',
    sc        float(255, 1) not null comment '评分',
    content   varchar(255)  not null comment '评论内容',
    approve   int default 0 not null comment '点赞数',
    calc_time datetime      not null comment '评论时间',
    constraint t_comment_ibfk_1
        foreign key (user_id) references t_user (id),
    constraint t_comment_ibfk_2
        foreign key (movie_id) references t_movie (id)
)
    charset = utf8;

create index movie_id
    on t_comment (movie_id);

create index user_id
    on t_comment (user_id);

create table t_comment_approve
(
    id         int auto_increment
        primary key,
    comment_id int not null,
    user_id    int not null,
    constraint t_comment_approve_ibfk_1
        foreign key (comment_id) references t_comment (id),
    constraint t_comment_approve_ibfk_2
        foreign key (user_id) references t_user (id)
)
    charset = utf8;

create index comment_id
    on t_comment_approve (comment_id);

create index user_id
    on t_comment_approve (user_id);

create table t_movie_wish
(
    id       int auto_increment
        primary key,
    user_id  int not null comment '用户id',
    movie_id int not null comment '电影id',
    constraint t_movie_wish_ibfk_1
        foreign key (user_id) references t_user (id),
    constraint t_movie_wish_ibfk_2
        foreign key (movie_id) references t_movie (id)
)
    charset = utf8;

create index movie_id
    on t_movie_wish (movie_id);

create index user_id
    on t_movie_wish (user_id);

create table t_order
(
    id          int auto_increment
        primary key,
    item_id     int            not null comment '商品id',
    item_type   varchar(255)   not null comment '类型：电影票、小吃',
    relate_id   int            not null comment '电影票：time_id；小吃：cinema_id',
    `describe`  varchar(255)   null comment '电影票：座位信息；小吃：数量',
    order_id    varchar(255)   not null comment '随机订单号',
    order_uid   int            not null comment '用户id',
    price       decimal(10, 2) not null comment '总价',
    create_time datetime       not null comment '下单时间',
    constraint t_order_ibfk_1
        foreign key (order_uid) references t_user (id)
)
    charset = utf8;

create index SeatInfo
    on t_order (item_type, relate_id);

create index UserOrder
    on t_order (item_type, order_uid);

create index order_uid
    on t_order (order_uid);

