package com.db.dbproject.service;

import com.db.dbproject.config.util.PageBean;
import com.db.dbproject.entity.Hall;
import com.db.dbproject.entity.Vo.AdminHall;
import com.db.dbproject.entity.Vo.HallWithType;
import com.db.dbproject.mapper.CinemaMapper;
import com.db.dbproject.mapper.HallMapper;
import com.db.dbproject.mapper.HallTypeMapper;
import com.db.dbproject.mapper.SeatMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class HallService {
    @Autowired
    private HallMapper hallMapper;
    @Autowired
    private CinemaMapper cinemaMapper;
    @Autowired
    private HallTypeMapper hallTypeMapper;
    @Autowired
    private SeatMapper seatMapper;

    public PageBean<AdminHall> getHalls(Integer pageNum, Integer limit, String keyword, Integer cinemaId) {
        PageHelper.startPage(pageNum, limit);
        // 根据hall类型，电影院名字，cinemaId三者共同查询
        List<Hall> halls = hallMapper.getHalls(keyword, cinemaId);
        List<AdminHall> adminHalls = new ArrayList<>();
        for (Hall hall : halls) {
            AdminHall adminHall = new AdminHall(hall);
            adminHall.setCinemaNm(cinemaMapper.selectById(hall.getCinemaId()).getNm());
            adminHall.setHallType(hallTypeMapper.selectById(hall.getHallTypeId()).getHalltype());
            adminHall.setSeats(seatMapper.getSeatByHallId(hall.getId()));
            adminHalls.add(adminHall);
        }
        PageInfo pageInfo = new PageInfo(halls);
        PageBean<AdminHall> page = new PageBean<>();
        page.setTr(pageInfo.getPages());
        page.setPc(pageInfo.getPageNum());
        page.setPs(pageInfo.getPageSize());
        page.setBeanList(adminHalls);
        return page;
    }

    public Hall getHallById(Integer hallId) {
        return hallMapper.selectById(hallId);
    }

    public void updateHall(Hall hall) {
        hallMapper.updateById(hall);
    }

    public void insertHall(Hall hall) {
        hallMapper.insert(hall);
    }

    public void deleteHall(Integer hallId) {
        hallMapper.deleteById(hallId);
    }

    public List<HallWithType> getHallByCinema(Integer cinemaId) {
        List<Hall> halls = hallMapper.getHallByCinema(cinemaId);
        List<HallWithType> hallWithTypes = new ArrayList<>();
        for (Hall hallItem : halls) {
            HallWithType hallWithType = new HallWithType(hallItem);
            hallWithType.setHall_type(hallTypeMapper.selectById(hallItem.getHallTypeId()).getHalltype());
            hallWithTypes.add(hallWithType);
        }
        return hallWithTypes;
    }
}
