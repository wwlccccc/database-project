package com.db.dbproject.service;

import com.db.dbproject.config.util.PageBean;
import com.db.dbproject.entity.User;
import com.db.dbproject.mapper.UserMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserMapper userMapper;

    public PageBean<User> getUsers(Integer pageNum,Integer limit,String keyword){
        PageHelper.startPage(pageNum,limit);
        List<User> userList = userMapper.getUsers(keyword);
        PageBean<User> page = new PageBean<>();
        PageInfo pageInfo = new PageInfo(userList);
        // 当前页码数
        page.setPc(pageInfo.getPageNum());
        // 所有页面的记录数量
        page.setTr(pageInfo.getPages());
        // 每个页面的记录数量
        page.setPs(pageInfo.getPageSize());
        // 总页面数
        if(pageInfo.getPageSize()!=0){
            page.setTp(pageInfo.getPages()/pageInfo.getPageSize());
        }
        else {
            page.setTp(0);
        }
        // 页面数据
        page.setBeanList(userList);
        return page;
    }

    public void banUser(Integer userId) {
        User user = userMapper.selectById(userId);
        user.setIsBanned(!user.getIsBanned());
        userMapper.updateById(user);
    }
}
