package com.db.dbproject.service;

import com.db.dbproject.entity.Vo.DataByCinemaAndMovie;
import com.db.dbproject.entity.Vo.DataCinemaAndMovie;
import com.db.dbproject.mapper.CinemaMapper;
import com.db.dbproject.mapper.DataMapper;
import com.db.dbproject.mapper.MovieMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class DataService {
    @Autowired
    private DataMapper dataMapper;

    @Autowired
    private MovieMapper movieMapper;

    @Autowired
    private CinemaMapper cinemaMapper;

    public List<DataCinemaAndMovie> getDataByCinemaAndMovie() {
        List<DataByCinemaAndMovie> data = dataMapper.getDataByCinemaAndMovie();
        List<DataCinemaAndMovie> dataCinemaAndMovies = new ArrayList<>();
        for (DataByCinemaAndMovie data1 : data) {
            DataCinemaAndMovie dataCinemaAndMovie = new DataCinemaAndMovie();
            dataCinemaAndMovie.setMovieId(data1.getMovieId());
            dataCinemaAndMovie.setCinemaId(data1.getCinemaId());
            dataCinemaAndMovie.setSumPrice(data1.getSumPrice());
            dataCinemaAndMovie.setCinema(cinemaMapper.selectById(data1.getCinemaId()));
            dataCinemaAndMovie.setMovie(movieMapper.selectById(data1.getMovieId()));
            dataCinemaAndMovies.add(dataCinemaAndMovie);
        }
        return dataCinemaAndMovies;
    }

    public List<DataCinemaAndMovie> getDataByCinema() {
        List<DataByCinemaAndMovie> data = dataMapper.getDataByCinema();
        List<DataCinemaAndMovie> dataCinemaAndMovies = new ArrayList<>();
        for (DataByCinemaAndMovie data1 : data) {
            DataCinemaAndMovie dataCinemaAndMovie = new DataCinemaAndMovie();
            dataCinemaAndMovie.setCinemaId(data1.getCinemaId());
            dataCinemaAndMovie.setSumPrice(data1.getSumPrice());
            dataCinemaAndMovie.setCinema(cinemaMapper.selectById(data1.getCinemaId()));
            dataCinemaAndMovies.add(dataCinemaAndMovie);
        }
        return dataCinemaAndMovies;
    }

    public List<DataCinemaAndMovie> getDataByMovie() {
        List<DataByCinemaAndMovie> data = dataMapper.getDataByMovie();
        List<DataCinemaAndMovie> dataCinemaAndMovies = new ArrayList<>();
        for (DataByCinemaAndMovie data1 : data) {
            DataCinemaAndMovie dataCinemaAndMovie = new DataCinemaAndMovie();
            dataCinemaAndMovie.setMovieId(data1.getMovieId());
            dataCinemaAndMovie.setSumPrice(data1.getSumPrice());
            dataCinemaAndMovie.setMovie(movieMapper.selectById(data1.getMovieId()));
            dataCinemaAndMovies.add(dataCinemaAndMovie);
        }
        return dataCinemaAndMovies;
    }
}
