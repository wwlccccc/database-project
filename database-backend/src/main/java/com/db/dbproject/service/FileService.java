package com.db.dbproject.service;

import com.aliyun.oss.OSS;
import com.aliyun.oss.model.ObjectMetadata;
import com.db.dbproject.config.util.Constant;
import com.db.dbproject.entity.Vo.ImageUploadBack;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.db.dbproject.config.util.Constant.bucketName;

@Service
public class FileService {

    @Autowired
    private OSS ossClient;

    /**
     * 存储文件到系统
     *
     * @param files 文件
     * @return 文件名
     */
    public List<ImageUploadBack> storeFile(MultipartFile[] files) {

        List<ImageUploadBack> list = new ArrayList<>();
        for (MultipartFile file : files) {
            String fileName = file.getOriginalFilename();
            // 获取文件类型
            String fileType = fileName.substring(fileName.lastIndexOf("."));
            // 新文件名称
            String newFileName = UUID.randomUUID().toString() + fileType;
            // 构建日期路径, 例如：OSS目标文件夹/2020/10/31/文件名
            String filePath = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
            // 文件上传的路径地址
            String uploadImgeUrl = Constant.fileHost + "/" + filePath + "/" + newFileName;
            // 获取文件输入流
            InputStream inputStream = null;
            try {
                inputStream = file.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            /**
             * 下面两行代码是重点坑：
             * 现在阿里云OSS 默认图片上传ContentType是image/jpeg
             * 也就是说，获取图片链接后，图片是下载链接，而并非在线浏览链接，
             * 因此，这里在上传的时候要解决ContentType的问题，将其改为image/jpg
             */
            ObjectMetadata meta = new ObjectMetadata();
            meta.setContentType("image/jpg");
            //文件上传至阿里云OSS
            ossClient.putObject(bucketName, uploadImgeUrl, inputStream, meta);
            /**
             * 注意：在实际项目中，文件上传成功后，数据库中存储文件地址
             */
            // 获取文件上传后的图片返回地址
            String url = "https://" + Constant.bucketName + "." + Constant.endPoint + "/" + uploadImgeUrl;
            list.add(new ImageUploadBack(fileName, url));
            System.out.println("url = " + url);
        }
        return list;
    }

    public List<ImageUploadBack> storeFiles(MultipartFile file) {

        List<ImageUploadBack> list = new ArrayList<>();
        String fileName = file.getOriginalFilename();
        // 获取文件类型
        String fileType = fileName.substring(fileName.lastIndexOf("."));
        // 新文件名称
        String newFileName = UUID.randomUUID().toString() + fileType;
        // 构建日期路径, 例如：OSS目标文件夹/2020/10/31/文件名
        String filePath = new SimpleDateFormat("yyyy/MM/dd").format(new Date());
        // 文件上传的路径地址
        String uploadImgeUrl = Constant.fileHost + "/" + filePath + "/" + newFileName;
        // 获取文件输入流
        InputStream inputStream = null;
        try {
            inputStream = file.getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
        }
        /**
         * 下面两行代码是重点坑：
         * 现在阿里云OSS 默认图片上传ContentType是image/jpeg
         * 也就是说，获取图片链接后，图片是下载链接，而并非在线浏览链接，
         * 因此，这里在上传的时候要解决ContentType的问题，将其改为image/jpg
         */
        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentType("video/mp4");
        //文件上传至阿里云OSS
        ossClient.putObject(bucketName, uploadImgeUrl, inputStream, meta);
        /**
         * 注意：在实际项目中，文件上传成功后，数据库中存储文件地址
         */
        // 获取文件上传后的图片返回地址
        String url = "https://" + Constant.bucketName + "." + Constant.endPoint + "/" + uploadImgeUrl;
        list.add(new ImageUploadBack(fileName, url));
        System.out.println("url = " + url);
        return list;
    }
}
