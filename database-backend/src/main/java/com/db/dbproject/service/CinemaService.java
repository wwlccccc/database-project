package com.db.dbproject.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.db.dbproject.config.exception.QQMapException;
import com.db.dbproject.config.util.Constant;
import com.db.dbproject.config.util.HttpClientUtil;
import com.db.dbproject.config.util.PageBean;
import com.db.dbproject.config.util.QQMapUtil;
import com.db.dbproject.entity.Cinema;
import com.db.dbproject.entity.CinemaBrand;
import com.db.dbproject.entity.Vo.AdminCinema;
import com.db.dbproject.entity.Vo.CinemaFilter;
import com.db.dbproject.entity.Vo.CinemaVo;
import com.db.dbproject.entity.Vo.SelectCity;
import com.db.dbproject.mapper.CinemaBrandMapper;
import com.db.dbproject.mapper.CinemaMapper;
import com.db.dbproject.mapper.HallTypeMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Select;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class CinemaService {
    @Autowired
    private RedisTemplate redisTemplate;
    @Autowired
    private CinemaMapper cinemaMapper;
    @Autowired
    private CinemaBrandMapper cinemaBrandMapper;
    @Autowired
    private HallTypeMapper hallTypeMapper;

    public List<Cinema> getCinemasByKeyWordAndDIstance(String keyword, double latitude, double longitude) {
        return cinemaMapper.getCinemasByKeyWordAndDIstance(latitude, latitude, longitude, keyword);
    }

    public PageBean<CinemaVo> getCinemas(Integer movieId, String date, Integer pageNum, Integer limit, SelectCity cityInfo, Integer brandId, Integer serviceId, Integer hallType, String selectRegion) {
        PageHelper.startPage(pageNum,limit);
        List<Cinema> cinemaList = cinemaMapper.getCinemas(movieId,date, cityInfo.getLatitude(),cityInfo.getLongitude(),cityInfo.getCityName(), brandId,serviceId,hallType,selectRegion);
        List<CinemaVo> cinemaVoList = new ArrayList<CinemaVo>();
        BigDecimal tmp = new BigDecimal(1000);
        for(Cinema cinema : cinemaList){
            cinema.setDistance(cinema.getDistance().divide(tmp).setScale(1,BigDecimal.ROUND_HALF_UP));
            CinemaVo cinemaVo = new CinemaVo();
            cinemaVo.setCinema(cinema);
            cinemaVo.setHallTypeList(hallTypeMapper.getHallTypeByCinemaId(cinema.getId()));
            cinemaVoList.add(cinemaVo);
        }
        PageInfo pageInfo = new PageInfo(cinemaList);
        PageBean<CinemaVo> page = new PageBean<CinemaVo>();
        page.setPc(pageInfo.getPageNum());
        page.setPs(pageInfo.getSize());
        page.setTr(pageInfo.getPages());
        page.setBeanList(cinemaVoList);
        return page;
    }

    public PageBean<AdminCinema> getCinema(Integer pageNum, Integer limit, String keyword) {
        PageHelper.startPage(pageNum,limit);
        List<Cinema> cinemaList = cinemaMapper.getCinema(keyword);
        PageBean<AdminCinema> page = new PageBean<>();
        PageInfo pageInfo = new PageInfo(cinemaList);
        page.setPc(pageInfo.getPageNum());
        page.setTr(pageInfo.getPages());
        page.setPs(pageInfo.getPageSize());
        List<AdminCinema> adminCinemas = new ArrayList<>();
        for(Cinema c : cinemaList){
            AdminCinema adminCinema = new AdminCinema(c);
            if(c.getBrandId()!=null)
                adminCinema.setBrandName(cinemaBrandMapper.getById(c.getBrandId()).getBrand());
            adminCinemas.add(adminCinema);
        }
        page.setBeanList(adminCinemas);
        return page;
    }

    public void deleteById(Integer cinemaId) {
        cinemaMapper.deleteById(cinemaId);
    }

    public void update(Cinema cinema1) {
        Cinema cm = cinemaMapper.selectById(cinema1.getId());
        if(!cm.getAddr().equalsIgnoreCase(cinema1.getAddr())){
            //地址修改了则更新lat，lng
            cinema1 = QQMapUtil.addrToLocat(cinema1);
        }
        cinemaMapper.updateById(cinema1);
    }

    public void insertCinema(Cinema cinema1) {
        cinemaMapper.insert(cinema1);
    }

    public Cinema getCinemaByName(String nm) {
        Cinema cinema = new Cinema();
        cinema.setNm(nm);
        // 查找一个名称符合的
        return cinemaMapper.selectOne(cinema);
    }

    public List<Cinema> getAllCinema() {
        return cinemaMapper.selectAll();
    }

    public CinemaFilter getfilters(SelectCity cityInfo) {
        String key = "filter,"+cityInfo.getCityName();
        CinemaFilter filter = (CinemaFilter) redisTemplate.opsForValue().get(key);
        if(filter!=null)
            return filter;
        CinemaFilter cinemaFilter = new CinemaFilter();
        CinemaBrand cinemaBrand = new CinemaBrand();
        cinemaBrand.setId(-1);
        cinemaBrand.setBrand("全部");
        cinemaFilter.setCinemaBrandList(cinemaBrandMapper.getAll());
        cinemaFilter.getCinemaBrandList().add(0,cinemaBrand);
        cinemaFilter.setHallTypeList(hallTypeMapper.getAll());
        //获取
        Map<String,String> map = new HashMap<>();
        map.put("key", Constant.QQ_MAP_KEY);
        map.put("keyword",cityInfo.getCityName());
        // 获取城市属性
        JSONObject object = JSONObject.parseObject(HttpClientUtil.doGet(Constant.QQ_MAP_SEARCH,map));
        try {
            // 获取城市id
            if(!object.getString("status").equals("0"))
                throw new QQMapException("QQ_MAP错误");
        } catch (QQMapException e) {
            e.printStackTrace();
        }
        JSONObject city = object.getJSONArray("result").getJSONArray(0).getJSONObject(0);
        map.remove("keyword");
        map.put("id",city.getString("id"));
        // 获取行政区域
        object = JSONObject.parseObject(HttpClientUtil.doGet(Constant.QQ_MAP_DISTRICT,map));
        try {
            if(!object.getString("status").equals("0"))
                throw new QQMapException("QQ_MAP错误");
        } catch (QQMapException e) {
            e.printStackTrace();
        }
        JSONArray tmp_district = object.getJSONArray("result").getJSONArray(0);
        List<String> district = new ArrayList<String>();
        district.add("全城");
        for(int i=0;i<tmp_district.size();i++){
            JSONObject tmp = tmp_district.getJSONObject(i);
            district.add(tmp.getString("fullname"));
        }
        // 加入缓存
        cinemaFilter.setDistrict(district);
        redisTemplate.opsForValue().set(key,cinemaFilter);
        redisTemplate.expire(key,1, TimeUnit.HOURS);
        return cinemaFilter;
    }

    public Cinema getCinemaById(Integer cinemaId) {
        return cinemaMapper.selectById(cinemaId);
    }
}
