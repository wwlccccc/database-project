package com.db.dbproject.service;

import com.db.dbproject.mapper.AdminMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminMenuService {
    @Autowired
    private AdminMenuMapper adminMenuMapper;

    // 根据用户id选择用户权限
    public List<String> selectPermissionByUserId(Integer userId){
        return adminMenuMapper.selectPermissionByUserId(userId);
    }
}
