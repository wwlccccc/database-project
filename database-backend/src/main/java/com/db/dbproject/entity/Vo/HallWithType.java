package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.Hall;

public class HallWithType extends Hall {
    private String hall_type;

    public HallWithType(Hall hall) {
        this.setId(hall.getId());
        this.setHallTypeId(hall.getHallTypeId());
        this.setCinemaId(hall.getCinemaId());
    }

    public String getHall_type() {
        return hall_type;
    }

    public void setHall_type(String hall_type) {
        this.hall_type = hall_type;
    }
}
