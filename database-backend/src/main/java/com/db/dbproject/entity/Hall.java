package com.db.dbproject.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

// 放映厅
@TableName("t_hall")
public class Hall extends Model<Hall> {
    private Integer id; // 编号id

    private Integer cinemaId; // 影院id ->外键

    private Integer hallTypeId; // 放映厅类型id->外键

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(Integer cinemaId) {
        this.cinemaId = cinemaId;
    }

    public Integer getHallTypeId() {
        return hallTypeId;
    }

    public void setHallTypeId(Integer hallTypeId) {
        this.hallTypeId = hallTypeId;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
