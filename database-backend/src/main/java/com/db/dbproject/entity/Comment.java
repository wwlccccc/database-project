package com.db.dbproject.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

// 评论部分
@TableName("t_comment")
public class Comment extends Model<Comment> {
    private Integer id;  // 编号id

    private Integer userId; // 用户id->外键

    private Integer movieId; // 电影id ->外键

    private Float sc; // 评分

    private String content; // 评论内容

    private Integer approve; // 点赞数，赞成数 这边请看comment-approve

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Date calcTime; // 评论时间

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public Float getSc() {
        return sc;
    }

    public void setSc(Float sc) {
        this.sc = sc;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content == null ? null : content.trim();
    }

    public Integer getApprove() {
        return approve;
    }

    public void setApprove(Integer approve) {
        this.approve = approve;
    }

    public Date getCalcTime() {
        return calcTime;
    }

    public void setCalcTime(Date calcTime) {
        this.calcTime = calcTime;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}