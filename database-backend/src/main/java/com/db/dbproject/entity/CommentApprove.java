package com.db.dbproject.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

// 赞成的评论部分，给评论点赞
@TableName("t_comment_approve")
public class CommentApprove extends Model<CommentApprove> {
    private Integer id; // 编号id

    private Integer commentId;  // 评论id --> 评论的外键

    private Integer userId;  // 用户id ->外键(用户)

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
