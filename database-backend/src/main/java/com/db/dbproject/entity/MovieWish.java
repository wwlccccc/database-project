package com.db.dbproject.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

// 期待看的电影
@TableName("t_movie_wish")
public class MovieWish extends Model<MovieWish> {
    private Integer id; // 编号

    private Integer userId;  // 用户编号id->外键

    private Integer movieId;  // 电影的编号id->外键

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}