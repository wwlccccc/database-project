package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.Hall;
import com.db.dbproject.entity.Seat;

import java.util.List;

// 放映厅的管理
public class AdminHall extends Hall {
    private String cinemaNm; // 影院名称
    private String hallType; // 放映厅类型
    private List<Seat> seats; // 影院座位集合

    public AdminHall(Hall hall){
        this.setId(hall.getId());
        this.setCinemaId(hall.getCinemaId());
        this.setHallTypeId(hall.getHallTypeId());
    }

    public String getCinemaNm() {
        return cinemaNm;
    }

    public void setCinemaNm(String cinemaNm) {
        this.cinemaNm = cinemaNm;
    }

    public String getHallType() {
        return hallType;
    }

    public void setHallType(String hallType) {
        this.hallType = hallType;
    }

    public List<Seat> getSeats() {
        return seats;
    }

    public void setSeats(List<Seat> seats) {
        this.seats = seats;
    }
}
