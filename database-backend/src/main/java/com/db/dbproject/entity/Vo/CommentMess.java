package com.db.dbproject.entity.Vo;

import java.util.List;

// 评论详细信息？
public class CommentMess {
    private List<CommentVo> comList;  // 评论信息集合
    private Integer total;  // 评论总数？？？

    public List<CommentVo> getComList() {
        return comList;
    }

    public void setComList(List<CommentVo> comList) {
        this.comList = comList;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}