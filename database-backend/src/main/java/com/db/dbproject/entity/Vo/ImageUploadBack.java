package com.db.dbproject.entity.Vo;

public class ImageUploadBack {

    private String name;
    private String url;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ImageUploadBack() {
    }

    public ImageUploadBack(String name, String url) {
        this.name = name;
        this.url = url;
    }
}
