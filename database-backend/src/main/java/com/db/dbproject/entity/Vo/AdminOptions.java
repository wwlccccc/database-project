package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.AdminRole;
import com.db.dbproject.entity.Cinema;
import com.db.dbproject.entity.HallType;
import com.db.dbproject.entity.Movie;

import java.util.List;

// 管理员对于所属电影院和电影的可选项
public class AdminOptions {
    private List<HallType> hallTypes; // 放映厅类型修改
    private List<Cinema> cinemas; // 影院集合
    private List<Movie> movies; // 电影集合
    private List<AdminRole> roles; // 角色权限

    public List<AdminRole> getRoles() {
        return roles;
    }

    public void setRoles(List<AdminRole> roles) {
        this.roles = roles;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public List<HallType> getHallTypes() {
        return hallTypes;
    }

    public void setHallTypes(List<HallType> hallTypes) {
        this.hallTypes = hallTypes;
    }

    public List<Cinema> getCinemas() {
        return cinemas;
    }

    public void setCinemas(List<Cinema> cinemas) {
        this.cinemas = cinemas;
    }
}