package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.Cinema;

// 管理员对于影院的操作可以修改品牌名称？
public class AdminCinema extends Cinema {
    private String brandName;  // 品牌名称

    public AdminCinema(Cinema cinema){
        this.setId(cinema.getId());
        this.setNm(cinema.getNm());
        this.setAddr(cinema.getAddr());
        this.setEndorse(cinema.getEndorse());
        this.setAllowrefund(cinema.getAllowrefund());
        this.setBrandId(cinema.getBrandId());
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }
}
