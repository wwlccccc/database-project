package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.Cinema;
import com.db.dbproject.entity.HallType;

import java.util.List;

// 电影院及其放映厅实体类
public class CinemaVo {
    private Cinema cinema;    //
    private List<HallType> HallTypeList;  // 放映厅类型集合

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public List<HallType> getHallTypeList() {
        return HallTypeList;
    }

    public void setHallTypeList(List<HallType> hallTypeList) {
        HallTypeList = hallTypeList;
    }
}

