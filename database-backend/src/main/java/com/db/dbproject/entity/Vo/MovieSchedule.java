package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.Days;
import com.db.dbproject.entity.Times;

import java.text.SimpleDateFormat;

// 电影的排片安排
public class MovieSchedule extends Times {
    private String movieNm;   // 这三个都是name，电影名
    private String cinemaNm;  // 影院名
    private String hallNm; // 放映厅名字
    private Days days; // 档期相关信息
    private String time;  // 时间信息

    public MovieSchedule(Times times){
        // 场次相关信息
        this.setId(times.getId());
        this.setDaysId(times.getDaysId());
        this.setPrice(times.getPrice());
        this.setHallId(times.getHallId());
        this.setStartTime(times.getStartTime());
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        this.time = format.format(times.getStartTime());
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getMovieNm() {
        return movieNm;
    }

    public void setMovieNm(String movieNm) {
        this.movieNm = movieNm;
    }

    public String getCinemaNm() {
        return cinemaNm;
    }

    public void setCinemaNm(String cinemaNm) {
        this.cinemaNm = cinemaNm;
    }

    public String getHallNm() {
        return hallNm;
    }

    public void setHallNm(String hallNm) {
        this.hallNm = hallNm;
    }

    public Days getDays() {
        return days;
    }

    public void setDays(Days days) {
        this.days = days;
    }
}