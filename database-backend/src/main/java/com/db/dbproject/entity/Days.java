package com.db.dbproject.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

// 电影的档期 且看操作
@TableName("t_days")
public class Days extends Model<Days> {
    private Integer id; // 编号id

    private Integer cinemaId; // 所属的影院的id  ->外键

    private Integer movieId; // 所属电影id  ->外键

    @JsonFormat(pattern="yyyy-MM-dd",timezone="GMT+8")
    private Date day;  // 档期日期

    @TableField(exist = false)
    private List<Times> times;  // 电影的档期集合？？ 猜测

    public List<Times> getTimes() {
        return times;
    }

    public void setTimes(List<Times> times) {
        this.times = times;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(Integer cinemaId) {
        this.cinemaId = cinemaId;
    }

    public Integer getMovieId() {
        return movieId;
    }

    public void setMovieId(Integer movieId) {
        this.movieId = movieId;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}