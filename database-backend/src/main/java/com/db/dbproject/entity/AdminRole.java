package com.db.dbproject.entity;

// 角色类型？管理员or 商家
public class AdminRole {
    private Integer id; // 编号id

    private String name;  // 角色类型名称

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }
}
