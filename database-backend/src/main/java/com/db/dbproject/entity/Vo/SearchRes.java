package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.Movie;

import java.util.List;

// 查询返回值
public class SearchRes {
    private List<Movie> movies;  // 电影集合
    private List<CinemaVo> cinemaVos; // 影院集合

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }

    public List<CinemaVo> getCinemaVos() {
        return cinemaVos;
    }

    public void setCinemaVos(List<CinemaVo> cinemaVos) {
        this.cinemaVos = cinemaVos;
    }
}