package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.Movie;

// 电影海报详细信息
public class MoviePhoto {
    private Movie movie;  //电影实体类
    private String[] photos;  // 对应的海报列表

    public MoviePhoto(Movie movie) {
        this.movie = movie;
        String tmp = movie.getPhotos();

        this.photos = tmp.split(",");
    }

    public Movie getMovie() {
        return movie;
    }

    public void setMovie(Movie movie) {
        this.movie = movie;
    }

    public String[] getPhotos() {
        return photos;
    }

    public void setPhotos(String[] photos) {
        this.photos = photos;
    }
}
