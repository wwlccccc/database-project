package com.db.dbproject.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;

// 影院名称 可以想象影院的连锁店类型  电影院的品牌
@TableName("t_cinema_brand")
public class CinemaBrand extends Model<Movie> {

    private Integer id;  // 编号id


    private String brand;  // 影院的宏观名字 比如万达影城


    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }


    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand == null ? null : brand.trim();
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
