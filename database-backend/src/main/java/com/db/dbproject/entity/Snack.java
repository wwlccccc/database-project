package com.db.dbproject.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

import java.io.Serializable;
import java.math.BigDecimal;


// 零食小吃类
@TableName("t_snack")
public class Snack extends Model<Snack> {
    private Integer id;  // 编号id

    private Integer cinemaId; // 所属影院->外键

    private String imageUrl;  // 显示的图片

    private String firstTitle;  // 套餐名称

    private String secondTitle;  // 详细描述

    private BigDecimal price;  // 价格

    private Integer curNumber;  // 已经售出的数量

    private Integer totalNumber;  // 总量的大小

    @TableField(exist = false)
    private String cinemaNm;   // ？应该是电影院的名称

    public String getCinemaNm() {
        return cinemaNm;
    }

    public void setCinemaNm(String cinemaNm) {
        this.cinemaNm = cinemaNm;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(Integer cinemaId) {
        this.cinemaId = cinemaId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl == null ? null : imageUrl.trim();
    }

    public String getFirstTitle() {
        return firstTitle;
    }

    public void setFirstTitle(String firstTitle) {
        this.firstTitle = firstTitle == null ? null : firstTitle.trim();
    }

    public String getSecondTitle() {
        return secondTitle;
    }

    public void setSecondTitle(String secondTitle) {
        this.secondTitle = secondTitle == null ? null : secondTitle.trim();
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Integer getCurNumber() {
        return curNumber;
    }

    public void setCurNumber(Integer curNumber) {
        this.curNumber = curNumber;
    }

    public Integer getTotalNumber() {
        return totalNumber;
    }

    public void setTotalNumber(Integer totalNumber) {
        this.totalNumber = totalNumber;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }
}