package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.Cinema;
import com.db.dbproject.entity.Movie;
import com.db.dbproject.entity.Snack;

import java.util.List;

// 电影院详情列表
public class CinemaDetail {
    private Cinema cinema;  // 电影院
    private List<Movie> movies;  // 电影集合
    private List<Snack> snacks;  // 小吃集合

    public List<Snack> getSnacks() {
        return snacks;
    }

    public void setSnacks(List<Snack> snacks) {
        this.snacks = snacks;
    }

    public Cinema getCinema() {
        return cinema;
    }

    public void setCinema(Cinema cinema) {
        this.cinema = cinema;
    }

    public List<Movie> getMovies() {
        return movies;
    }

    public void setMovies(List<Movie> movies) {
        this.movies = movies;
    }
}
