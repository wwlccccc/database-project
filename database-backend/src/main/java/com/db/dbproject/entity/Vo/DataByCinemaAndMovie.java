package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.Cinema;
import com.db.dbproject.entity.Movie;

public class DataByCinemaAndMovie {
    private int cinemaId;
    private int movieId;
    private double sumPrice;

    public int getCinemaId() {
        return cinemaId;
    }

    public void setCinemaId(int cinemaId) {
        this.cinemaId = cinemaId;
    }

    public int getMovieId() {
        return movieId;
    }

    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }

    public double getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(double sumPrice) {
        this.sumPrice = sumPrice;
    }
}
