package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.Comment;

// 管理员对于评论的操作
public class AdminComment extends Comment {
    private String userName; // 用户名->外键
    private String movieName; // 电影名->外键

    public AdminComment(Comment comment){
        this.setApprove(comment.getApprove());
        this.setCalcTime(comment.getCalcTime());
        this.setContent(comment.getContent());
        this.setMovieId(comment.getMovieId());
        this.setId(comment.getId());
        this.setSc(comment.getSc());
        this.setUserId(comment.getUserId());
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMovieName() {
        return movieName;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }
}
