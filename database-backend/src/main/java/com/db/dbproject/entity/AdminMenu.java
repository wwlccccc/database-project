package com.db.dbproject.entity;

// 管理员菜单许可
public class AdminMenu {
    private Integer id;  // 编号id

    private String permission; // 用户的具体权限

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPermission() {
        return permission;
    }

    public void setPermission(String permission) {
        this.permission = permission == null ? null : permission.trim();
    }
}
