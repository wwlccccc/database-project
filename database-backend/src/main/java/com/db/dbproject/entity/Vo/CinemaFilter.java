package com.db.dbproject.entity.Vo;

import com.db.dbproject.entity.CinemaBrand;
import com.db.dbproject.entity.HallType;

import java.util.List;

// 电影院类型筛选
public class CinemaFilter {
    private List<String> district;    // 不知道啥属性
    private List<CinemaBrand> cinemaBrandList;  // 电影院品牌列表
    private List<HallType> hallTypeList; // 放映厅类型

    public List<String> getDistrict() {
        return district;
    }

    public void setDistrict(List<String> district) {
        this.district = district;
    }

    public List<CinemaBrand> getCinemaBrandList() {
        return cinemaBrandList;
    }

    public void setCinemaBrandList(List<CinemaBrand> cinemaBrandList) {
        this.cinemaBrandList = cinemaBrandList;
    }

    public List<HallType> getHallTypeList() {
        return hallTypeList;
    }

    public void setHallTypeList(List<HallType> hallTypeList) {
        this.hallTypeList = hallTypeList;
    }
}
