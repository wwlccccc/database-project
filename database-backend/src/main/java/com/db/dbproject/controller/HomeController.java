package com.db.dbproject.controller;

import com.alibaba.fastjson.JSONObject;
import com.db.dbproject.config.util.PageBean;
import com.db.dbproject.config.util.Result;
import com.db.dbproject.entity.*;
import com.db.dbproject.entity.Vo.CinemaVo;
import com.db.dbproject.entity.Vo.MoviePhoto;
import com.db.dbproject.entity.Vo.SearchRes;
import com.db.dbproject.entity.Vo.SelectCity;
import com.db.dbproject.mapper.HallMapper;
import com.db.dbproject.mapper.HallTypeMapper;
import com.db.dbproject.service.BannerService;
import com.db.dbproject.service.CinemaService;
import com.db.dbproject.service.MovieService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.elasticsearch.common.geo.GeoDistance;
import org.elasticsearch.common.unit.DistanceUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/home")
public class HomeController {
    private final static Logger logger = LoggerFactory.getLogger(HomeController.class);
    @Autowired
    private BannerService bannerService;
    @Autowired
    private MovieService movieService;
    @Autowired
    private CinemaService cinemaService;
    @Autowired
    private HallTypeMapper hallTypeMapper;
    // 轮播图
    @GetMapping("/getBannerList")
    public Result getBannerList() {
        List<Banner> list = bannerService.getBannerList();
        return new Result(list);
    }

    // 获取首页电影热映列表
    @GetMapping("/getMovieList")
    public Result getMovieList(@RequestParam("pageNum") Integer pageNum,
                               @RequestParam("limit") Integer limit,
                               @RequestParam(value = "addr", required = false) String addr) {
        PageBean<Movie> pageBean = movieService.getList(pageNum, limit, addr);
        // 转化为json对象
        return new Result(JSONObject.toJSON(pageBean));
    }

    //获取首页电影待映列表
    @GetMapping("/getMovieWait")
    public Result getMovieWait(@RequestParam("pageNum") Integer pageNum,
                               @RequestParam("limit") Integer limit) {
        PageBean<Movie> pageBean = movieService.getWait(pageNum, limit);
        return new Result(JSONObject.toJSON(pageBean));
    }

    //获取首页电影待映列表(近期受欢迎)
    @GetMapping("/getMoviePop")
    public Result getMoviePop() {
        List<Movie> list = movieService.getMoviePop();
        return new Result(JSONObject.toJSON(list));
    }

    // 搜索服务
    @PostMapping("/search")
    public Result search(@RequestParam("keyword") String keyword,
                         @RequestParam("stype") Integer stype,
                         @RequestParam("selectCity") String selectCity) {
        // selectCity数据格式化解析
        JSONObject object = JSONObject.parseObject(selectCity);
        SelectCity cityInfo = object.toJavaObject(SelectCity.class);
        SearchRes searchRes = new SearchRes();
        if (stype == -1) {
            // 两者均查询movie和cinema
            List<Movie> movies = movieService.findByNmLikeOrDirLikeOrCatLikeOrDraLikeOrStarContains(keyword);
            List<CinemaVo> cinemas = searchCinema(keyword, cityInfo);
            searchRes.setMovies(movies);
            searchRes.setCinemaVos(cinemas);
        } else if (stype == 2) {
            // 仅仅查询cinema这个部分
            List<CinemaVo> cinemas = searchCinema(keyword, cityInfo);
            searchRes.setCinemaVos(cinemas);
        }
        // 返回响应数据
        return new Result(searchRes);
    }

    private List<CinemaVo> searchCinema(String keyword, SelectCity cityInfo) {
        // 查询操作
        List<Cinema> cinemas = cinemaService.getCinemasByKeyWordAndDIstance(keyword,
                cityInfo.getLatitude().doubleValue(),
                cityInfo.getLongitude().doubleValue());
        // 含有影厅的电影院查询操作，有难度
        // 查询电影院及其相关的影厅
        List<CinemaVo> cinemaVoList = new ArrayList<CinemaVo>();
        for(Cinema cinema : cinemas){
            CinemaVo cinemaVo = new CinemaVo();
            cinemaVo.setCinema(cinema);
            cinemaVo.setHallTypeList(hallTypeMapper.getHallTypeByCinemaId(cinema.getId()));
            cinemaVoList.add(cinemaVo);
        }
        System.out.println(cinemaVoList);
        return cinemaVoList;
    }

    //电影详情,客户端来获取的
    @GetMapping("/getMovieById/{id}")
    public Result getMoviePop(@PathVariable("id") Integer id){
        MoviePhoto moviePhoto  = movieService.getMovieById(id);
        if(moviePhoto==null)
            return new Result(Result.ERROR,"未找到");
        return new Result(moviePhoto);
    }

    //判断是否想看,判断我是否想看
    @GetMapping("/getIsWish")
    public Result getIsWish(@RequestParam("movieId") Integer movieId){
        Subject subject = SecurityUtils.getSubject();
        if(!subject.isAuthenticated())
            return new Result(Result.NOAUTHC,"未登录");
        User user = (User)subject.getPrincipal();
        MovieWish movieWish = movieService.getIsWish(user.getId(),movieId);
        if(movieWish==null)
            return new Result(false);
        else
            return new Result(true);
    }

    //更新想看人数，这里加入了我
    @PostMapping("/updateWish")
    public Result updateWish(@RequestParam("movieId") Integer movieId){
        Subject subject = SecurityUtils.getSubject();
        if(!subject.isAuthenticated())
            return new Result(Result.NOAUTHC,"未登录");
        User user = (User)subject.getPrincipal();
        movieService.updateWish(user.getId(),movieId);
        return new Result(Result.SUCCESS,"成功");
    }

    //获取同类型的电影列表
    @GetMapping("/getLikeMovieList")
    public Result getLikeMovieList(@RequestParam("movieId") Integer movieId,
                                   @RequestParam("cat") String cat){
        Subject subject = SecurityUtils.getSubject();
        User user = (User)subject.getPrincipal();
//        if(user == null) {
//            List<Movie> movies = movieService.getLikeMovieList(movieId, cat, 3);
//            return new Result(movies);
//        }else {
//            Set<Movie> movies = movieService.userBasedRecommender(movieId,cat,user.getId(), 4);
//            return new Result(movies);
//        }
        List<Movie> movies = movieService.getLikeMovieList(movieId, cat, 3);
        return new Result(movies);
    }
}
