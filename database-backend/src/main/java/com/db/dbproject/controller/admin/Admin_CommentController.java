package com.db.dbproject.controller.admin;

import com.db.dbproject.config.util.PageBean;
import com.db.dbproject.config.util.Result;
import com.db.dbproject.entity.Vo.AdminComment;
import com.db.dbproject.service.CommentService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/admin/comment")
@RequiresPermissions("评论管理")
public class Admin_CommentController {
    private final static Logger logger = LoggerFactory.getLogger(Admin_CommentController.class);
    @Autowired
    private CommentService commentService;

    //获取评论列表
    @GetMapping("/getComments")
    public Result getComments(@RequestParam("pageNum") Integer pageNum,
                              @RequestParam("limit") Integer limit,
                              @RequestParam(value = "keyword",required = false) String keyword){
        PageBean<AdminComment> adminCommentPageBean = commentService.getComments(pageNum,limit,keyword);
        return new Result(adminCommentPageBean);
    }

    //删除评论
    @PostMapping("/deleteComment")
    public Result deleteComment(@RequestBody HashMap<String,Integer> map){
        Integer commentId = map.get("commentId");
        commentService.deleteById(commentId);
        return new Result(commentId);
    }

}
