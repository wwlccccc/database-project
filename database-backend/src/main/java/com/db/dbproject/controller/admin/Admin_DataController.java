package com.db.dbproject.controller.admin;

import com.db.dbproject.config.util.PageBean;
import com.db.dbproject.config.util.Result;
import com.db.dbproject.entity.Vo.AdminComment;
import com.db.dbproject.service.DataService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/data")

public class Admin_DataController {
    private final static Logger logger = LoggerFactory.getLogger(Admin_DataController.class);

    @Autowired
    private DataService dataService;

    // 获取电影院和电影数据
    @GetMapping("/getCinemaAndMovie")
    public Result getDataByCinemaAndMovie(){
        return new Result(dataService.getDataByCinemaAndMovie());
    }

    // 获取电影院和电影数据
    @GetMapping("/getCinema")
    public Result getDataByCinema(){
        return new Result(dataService.getDataByCinema());
    }
    // 获取电影院和电影数据
    @GetMapping("/getMovie")
    public Result getDataByMovie(){
        return new Result(dataService.getDataByMovie());
    }

}
