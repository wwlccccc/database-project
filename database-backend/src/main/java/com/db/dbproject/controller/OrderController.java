package com.db.dbproject.controller;

import com.db.dbproject.config.util.Result;
import com.db.dbproject.controller.admin.Admin_UserController;
import com.db.dbproject.entity.Order;
import com.db.dbproject.entity.User;
import com.db.dbproject.entity.Vo.OrderItem;
import com.db.dbproject.service.OrderService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {
    private final static Logger logger = LoggerFactory.getLogger(OrderController.class);
    @Autowired
    private OrderService orderService;

    @PostMapping("/addOrder")
    public Result addOrder(@RequestParam("itemId") Integer itemId,
                           @RequestParam("itemType") String itemType,
                           @RequestParam("relateId") Integer relateId,
                           @RequestParam("orderId") String orderId,
                           @RequestParam("price") Double price,
                           @RequestParam("seat") String seat){
        Subject subject = SecurityUtils.getSubject();
        if(!subject.isAuthenticated())
            return new Result(Result.NOAUTHC,"未登录");
        User user = (User)subject.getPrincipal();
        Order order = new Order();
        order.setItemId(itemId);
        order.setItemType(itemType);
        order.setRelateId(relateId);
        order.setOrderId(orderId);
        order.setPrice(new BigDecimal(price));
        order.setOrderUid(user.getId());
        order.setCreateTime(new Date());
        OrderItem orderItem = orderService.addOrder(order,seat);
        return new Result(orderItem);
    }

    // 获取购票信息
    @GetMapping("/getMovieOrder")
    public Result getMovieOrder(){
        Subject subject = SecurityUtils.getSubject();
        if(!subject.isAuthenticated())
            return new Result(Result.NOAUTHC,"未登录");
        User user = (User)subject.getPrincipal();
        List<OrderItem> orderList = orderService.getMovieOrderByUser(user.getId());
        return new Result(orderList);
    }

    // 获取订单信息
    @GetMapping("/getSnackOrder")
    public Result getSnackOrder(){
        Subject subject = SecurityUtils.getSubject();
        if(!subject.isAuthenticated())
            return new Result(Result.NOAUTHC,"未登录");
        User user = (User)subject.getPrincipal();
        List<OrderItem> orderList = orderService.getSnackOrderByUser(user.getId());
        return new Result(orderList);
    }

}
