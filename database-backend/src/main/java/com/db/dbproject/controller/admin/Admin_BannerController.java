package com.db.dbproject.controller.admin;

import com.db.dbproject.config.util.PageBean;
import com.db.dbproject.config.util.Result;
import com.db.dbproject.entity.Banner;
import com.db.dbproject.service.BannerService;
import net.sf.json.JSONObject;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping("/admin/banner")
@RequiresPermissions("广告管理")
public class Admin_BannerController {
    private final static Logger logger = LoggerFactory.getLogger(Admin_BannerController.class);

    @Autowired
    private BannerService bannerService;

    // 获取广告列表
    @GetMapping("/getBanner")
    public Result getBanner(@RequestParam("pageNum") Integer pageNum,
                            @RequestParam("limit") Integer limit,
                            @RequestParam(value = "keyword",required = false) String keyword){
        PageBean<Banner> bannerPageBean = bannerService.getBanner(pageNum,limit,keyword);
        return new Result(bannerPageBean);
    }

    // 新增广告
    @PostMapping("/addBanner")
    public Result addBanner(@RequestBody String banner) {
        String s = StringEscapeUtils.unescapeJava(banner);
        JSONObject object = JSONObject.fromObject(s);
        Banner banner1 = (Banner) JSONObject.toBean(object.getJSONObject("banner"), Banner.class);
        if(banner1.getId()!=null){
            // 更新
            bannerService.update(banner1);
        }else{
            //添加
            bannerService.insert(banner1);
        }
        return new Result(banner1);
    }

    @PostMapping("/deleteBanner")
    public Result deleteBanner(@RequestBody HashMap<String,Integer> map){
        Integer bannerId = map.get("bannerId");
        bannerService.deleteBanner(bannerId);
        return new Result(bannerId);
    }
}
