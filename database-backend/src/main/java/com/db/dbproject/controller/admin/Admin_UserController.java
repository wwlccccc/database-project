package com.db.dbproject.controller.admin;

import com.db.dbproject.config.shiro.AdminToken;
import com.db.dbproject.config.util.PageBean;
import com.db.dbproject.config.util.Result;
import com.db.dbproject.entity.AdminUser;
import com.db.dbproject.entity.User;
import com.db.dbproject.service.AdminUserService;
import com.db.dbproject.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import java.util.HashMap;

@RestController
@RequestMapping("/admin/user")
public class Admin_UserController {
    private final static Logger logger = LoggerFactory.getLogger(Admin_UserController.class);
    @Autowired
    private UserService userService;
    @Autowired
    private AdminUserService adminUserService;

    // 后台用户登录部分
    @PostMapping("/login")
    public Result login(@RequestBody HashMap<String,String> map){
        String username = map.get("username");
        String password = map.get("password");
        System.out.println(username+"username");

        // shiro权限认证开始
        Subject subject = SecurityUtils.getSubject();
        // 传递账号和密码
        AdminToken usernamePasswordToken = new AdminToken();
        usernamePasswordToken.setUsername(username);
        usernamePasswordToken.setPassword(password.toCharArray());
        try{
            subject.login(usernamePasswordToken);
        }
        catch (Exception e){
            AdminUser adminUser = adminUserService.getByUserName(username);
            if(adminUser==null){
                return new Result(Result.ERROR,"用户未注册，请联系管理员添加!");
            }
            return new Result(Result.NOAUTHC,"用户名或密码错误!");
        }
        AdminUser adminUser = (AdminUser) subject.getPrincipal();
        int userId = ((AdminUser) subject.getPrincipal()).getId();
        System.out.println("userId"+userId);
        AdminUser adminUser1 = adminUserService.selectUserById(userId);
        return new Result(adminUser1);
    }

    // 获取用户列表
    @GetMapping("/getUsers")
    @RequiresPermissions("用户管理")
    public Result getUsers(@RequestParam("pageNum")Integer pageNum,
                           @RequestParam("limit")Integer limit,
                           @RequestParam(value = "keyword",required = false)String keyword){
        PageBean<User> userPageBean = userService.getUsers(pageNum,limit,keyword);
        return new Result(userPageBean);
    }
    //禁用用户
    @PostMapping("/banUser")
    @RequiresPermissions("用户管理")
    public Result banUser(@RequestBody HashMap<String,Integer> map){
        Integer userId = map.get("userId");
        userService.banUser(userId);
        return new Result(userId);
    }
}
