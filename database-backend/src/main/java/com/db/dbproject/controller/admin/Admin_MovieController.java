package com.db.dbproject.controller.admin;

import com.alibaba.fastjson.JSONObject;
import com.db.dbproject.config.util.PageBean;
import com.db.dbproject.config.util.Result;
import com.db.dbproject.entity.Movie;
import com.db.dbproject.entity.Vo.ImageUploadBack;
import com.db.dbproject.service.FileService;
import com.db.dbproject.service.MovieService;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/movie")
public class Admin_MovieController {
    private final static Logger logger = LoggerFactory.getLogger(Admin_MovieController.class);

    @Autowired
    private MovieService movieService;
    @Autowired
    private FileService fileService;

    @PostMapping("/addMoive")
    @RequiresPermissions("电影管理")
    public Result addMovie(@RequestBody String movie){
        // 处理序列化
        String s = StringEscapeUtils.unescapeJava(movie);
        Movie movie1 = JSONObject.parseObject(s).getJSONObject("movie").toJavaObject(Movie.class);
        StringBuffer stringBuffer = new StringBuffer();
        for(String ss : movie1.getPhotosList()){
            stringBuffer.append(ss+",");
        }
        movie1.setPhotos(stringBuffer.substring(0,stringBuffer.length()-1));
        //修改
        if(movie1.getId()!=null){
            movieService.update(movie1);
        }else{
            //添加新的电影
            Movie mv = movieService.getMovieByName(movie1.getNm());
            if(mv!=null)
                return new Result(Result.ERROR,"该电影已存在");
            movieService.insertMovie(movie1);
        }
        return new Result(movie);
    }

    @PostMapping("/deleteMovie")
    @RequiresPermissions("电影管理")
    public Result deleteMovie(@RequestBody HashMap<String, String> map){
        Integer movieId = Integer.parseInt(map.get("movieId"));
        movieService.deleteById(movieId);
        return new Result(movieId);
    }

    @GetMapping("/getMovies")
    @RequiresPermissions("电影管理")
    public Result getMovies(@RequestParam("pageNum") Integer pageNum,
                            @RequestParam("limit") Integer limit,
                            @RequestParam(value = "keyword",required = false) String keyword){
        PageBean<Movie> moviePageBean = movieService.getMovies(pageNum,limit,keyword);
        return new Result(moviePageBean);
    }


    // 文件上传功能
    @PostMapping(value = "/upLoadFile",produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @RequiresPermissions("上传文件")
    public Result upLoadFile(@RequestParam(value = "images") MultipartFile[] images) {
        if (images != null && images.length > 0) {
            return new Result(fileService.storeFile(images));
        } else {
            return new Result(Result.ERROR, "未知图片类型!");
        }
    }
    @PostMapping("/upLoadFiles")
    @RequiresPermissions("上传文件")
    public Result upLoadFile(@RequestParam(value = "images",required = false) MultipartFile[] img,
                             @RequestParam(value = "videoImg",required = false) MultipartFile[] videoImg,
                             @RequestParam(value = "video",required = false) MultipartFile video){
        Map<String,String> res = new HashMap<>();
        if(img!=null){
            List<ImageUploadBack> fileNameVideoImg = fileService.storeFile(img);
            res.put("img",fileNameVideoImg.get(0).getUrl());
        }
        if(videoImg!=null){
            List<ImageUploadBack> fileNameVideoImg = fileService.storeFile(videoImg);
            res.put("videoImg",fileNameVideoImg.get(0).getUrl());
        }
        if(video!=null){
            List<ImageUploadBack> fileNameVideoImg = fileService.storeFiles(video);
            res.put("video",fileNameVideoImg.get(0).getUrl());
        }
        return new Result(res);
    }

}
