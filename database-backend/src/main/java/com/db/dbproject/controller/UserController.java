package com.db.dbproject.controller;

import com.alibaba.druid.util.HttpClientUtils;
import com.alibaba.fastjson.JSONObject;
import com.db.dbproject.config.shiro.WxToken;
import com.db.dbproject.config.util.Constant;
import com.db.dbproject.config.util.HttpClientUtil;
import com.db.dbproject.config.util.Result;
import com.db.dbproject.entity.User;
import org.apache.commons.net.util.SubnetUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;


@RestController
@RequestMapping("/user")
public class UserController {
    // 日志记录
    private final static Logger logger = LoggerFactory.getLogger(UserController.class);

    // 用户微信登录
    @PostMapping("/wxLogin")
    public Result wxLogin(@RequestParam("code")String code,
                          @RequestParam("nickName")String nickName,
                          @RequestParam("avatarUrl")String avatarUrl,
                          @RequestParam("gender")String gender){
        // 发送请求完成登录操作
        Map<String,String> param = new HashMap<>();
        param.put("appid", Constant.WX_LOGIN_APPID);
        param.put("secret",Constant.WX_LOGIN_SECRET);
        param.put("js_code",code);
        param.put("grant_type",Constant.WX_LOGIN_GRANT_TYPE);
        // 构造http请求，向微信服务器发送相关信息
        String wxResult = HttpClientUtil.doGet(Constant.WX_LOGIN_URL,param);
        // 得到响应的请求结果
        System.out.println(wxResult);
        JSONObject jsonObject = JSONObject.parseObject(wxResult);
        // 获取session_key和open_id
        // String session_key = jsonObject.get("session_key").toString();
        String open_id = jsonObject.get("openid").toString();


        // 根据返回的user实体类，判断用户是否是新用户，不是的话，更新最新登录时间，是的话，将用户信息存到数据库
        // shiro权限认证框架
        Subject subject = SecurityUtils.getSubject();
        User user = new User();
        user.setAvatarUrl(avatarUrl);
        user.setNickName(nickName);
        if(gender.equals("1")){
            user.setGender("男");
        }else{
            user.setGender("女");
        }
        user.setLastLogin(new Date());
        user.setOpenId(open_id);
        // 注意该部分通过shiro认证框架实现
        WxToken wxToken = new WxToken(user);
        subject.login(wxToken); // 执行登录方法，有异常需要捕获异常

        // 将结果封装返回小程序
        Map<String,String> result = new HashMap<>();
        result.put("token",subject.getSession().getId().toString());

        return new Result(result);
    }

    // 验证token是否过期了
    @GetMapping("/isAuth")
    public Result isAuth(){
        Subject subject = SecurityUtils.getSubject();
        if(!subject.isAuthenticated()){
            return new Result(Result.NOAUTHC,"未登录");
        }
        return new Result();
    }
}
