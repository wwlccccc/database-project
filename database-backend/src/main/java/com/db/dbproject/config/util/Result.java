package com.db.dbproject.config.util;

import java.io.Serializable;

public class Result implements Serializable {
    private static final long serialVersionUID = -4800793124936904868L;
    public static final int SUCCESS=200;  //  成功
    public static final int ERROR=201;  // 失败
    public static final int NOAUTHC=202; // 未用户权限认证

// 表示返回值状态
    private int state;
//    成功时候,返回的JSON数据
    private Object data;
//    是错误时候的错误消息
    private String message;

    public Result() {
    }


    // 返回result，通用
    public Result(int state,String message) {
        this.state = state;
        this.message = message;
    }

    // 返回错误信息
    public Result(Throwable e){
        state = ERROR;
        data=null;
        message=e.getMessage();
    }

    // 返回响应数据
    public Result(Object data){
        state = SUCCESS;
        this.data=data;
        message="";
    }

    public int getState() {
        return state;
    }


    public void setState(int state) {
        this.state = state;
    }


    public Object getData() {
        return data;
    }


    public void setData(Object data) {
        this.data = data;
    }


    public String getMessage() {
        return message;
    }


    public void setMessage(String message) {
        this.message = message;
    }


    @Override
    public String toString() {
        return "JsonResult [state=" + state + ", data=" + data + ", message=" + message + "]";
    }

}
