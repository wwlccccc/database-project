package com.db.dbproject.config.shiro;

import com.db.dbproject.config.exception.AuthException;
import com.db.dbproject.entity.AdminUser;
import com.db.dbproject.service.AdminMenuService;
import com.db.dbproject.service.AdminUserService;
import org.apache.hadoop.hbase.client.Admin;
import org.apache.shiro.authc.*;

import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class AdminRealm extends AuthorizingRealm {
    @Autowired
    private AdminUserService adminUserService;
    @Autowired
    private AdminMenuService adminMenuService;

    // 用户授权
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        System.out.println("用户授权");
        AdminUser adminUser = (AdminUser) principalCollection.getPrimaryPrincipal();
        // 菜单权限
        List<String> userPermissions = adminMenuService.selectPermissionByUserId(adminUser.getId());
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        // 赋予权限集合
        info.addStringPermissions(userPermissions);
        return info;
    }

    // 用户认证
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        System.out.println("用户认证");
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        String username = token.getUsername();
        AdminUser adminUser = adminUserService.getByUserName(username);
        if(adminUser == null){
            try {
                throw new AuthException("该用户不存在");
            } catch (AuthException e) {
                e.printStackTrace();
            }
        }
        Object credentials = adminUser.getPassword();
        AuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(adminUser,credentials,this.getClass().getSimpleName());
        return authenticationInfo;
    }

    @Override
    public boolean supports(AuthenticationToken authenticationToken) {
        // 设置此Realm支持的Token
        return authenticationToken != null && (authenticationToken instanceof AdminToken );
    }
}
