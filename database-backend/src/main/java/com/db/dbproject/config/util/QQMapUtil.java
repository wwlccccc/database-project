package com.db.dbproject.config.util;

import com.alibaba.fastjson.JSONObject;
import com.db.dbproject.config.exception.QQMapException;
import com.db.dbproject.entity.Cinema;
import org.apache.commons.lang.StringEscapeUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class QQMapUtil {
    public static Cinema addrToLocat(Cinema cinema) {
        Map<String,String> map = new HashMap<>();
        map.put("key", Constant.QQ_MAP_KEY);
        map.put("address",cinema.getAddr());
        String qq_res = HttpClientUtil.doGet(Constant.QQ_MAP_TO,map);
        // 反编码处理
        JSONObject object = JSONObject.parseObject(StringEscapeUtils.unescapeJava(qq_res));
        try {
            if(!object.getString("status").equals("0")) {
                Map<String,String> map2 = new HashMap<>();
                map2.put("ak", Constant.BAIDU_MAP_AK);
                map2.put("address",cinema.getAddr());
                map2.put("output","json");
                String baidu_res = HttpClientUtil.doGet(Constant.BAIDU_MAP_TO,map2);
                object = JSONObject.parseObject(StringEscapeUtils.unescapeJava(baidu_res));
                if(!object.getString("status").equals("0")) {
                    // 两个平台都解析不出来，说明解析错误，可以试试三个平台
                    throw new QQMapException("地址解析错误");
                }
            }
        } catch (QQMapException e) {
            e.printStackTrace();
        }
        // 地址编码解析
        JSONObject result = object.getJSONObject("result");
        if(result.getJSONObject("address_components") != null) {
            // 腾讯地图解析地址实现，后续详细地址没有解析 只是用了省市
            String[] res = new String[3];
            JSONObject tmp = result.getJSONObject("address_components");
            res[0] = "";
            if (!cinema.getAddr().contains(tmp.getString("province")))
                res[0] += tmp.getString("province");
            if (!cinema.getAddr().contains(tmp.getString("city")))
                res[0] += tmp.getString("city");
            res[0] += cinema.getAddr();
            res[1] = result.getJSONObject("location").getString("lat");
            res[2] = result.getJSONObject("location").getString("lng");
            cinema.setAddr(res[0]);
            cinema.setLatitude(new BigDecimal(res[1]));
            cinema.setLongitude(new BigDecimal(res[2]));
        }
        else {
            // 百度地图的逆地址编码
            cinema.setLatitude(new BigDecimal(result.getJSONObject("location").getString("lat")));
            cinema.setLongitude(new BigDecimal(result.getJSONObject("location").getString("lng")));
        }

        return cinema;
    }
}
