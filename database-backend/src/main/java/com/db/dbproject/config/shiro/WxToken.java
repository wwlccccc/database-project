package com.db.dbproject.config.shiro;

import com.db.dbproject.entity.User;
import org.apache.shiro.authc.UsernamePasswordToken;

import java.io.Serializable;

public class WxToken extends UsernamePasswordToken implements Serializable {
    private static final long serialVersionUID = 4812793519945855483L;
    private User user;

    public WxToken(User user){
        this.user = user;
    }
    public String getOpenId(){
        return user.getOpenId();
    }

    @Override
    public Object getPrincipal(){
        return getOpenId();
    }
    @Override
    public Object getCredentials(){
        return "ok";
    }

    public User getUser(){
        return user;
    }

}
