package com.db.dbproject.config.exception;

import java.io.File;

public class FileException extends RuntimeException{
    public FileException(String message){super(message);}
    public FileException(String message,Throwable cause){super(message, cause);}
}
