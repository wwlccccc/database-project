package com.db.dbproject.config.exception;

public class QQMapException extends Exception{
    public QQMapException(String msg){
        super(msg);
    }
}
