package com.db.dbproject.config.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import org.springframework.context.annotation.Bean;

public interface Constant {

    /*
     * 微信相关
     */
    // 请求的网址
    public static final String WX_LOGIN_URL = "https://api.weixin.qq.com/sns/jscode2session";
    // 你的appid
    public static final String WX_LOGIN_APPID = "";
    // 你的密匙
    public static final String WX_LOGIN_SECRET = "";
    // 固定参数
    public static final String WX_LOGIN_GRANT_TYPE = "authorization_code";

    // 腾讯地图相关参数
    // key
    public static final String QQ_MAP_KEY = "";
    // url
    public static final String QQ_MAP_TO = "https://apis.map.qq.com/ws/geocoder/v1/";
    //搜索城市id
    public static final String QQ_MAP_SEARCH = "https://apis.map.qq.com/ws/district/v1/search";
    //获取行政划分
    public static final String QQ_MAP_DISTRICT = "https://apis.map.qq.com/ws/district/v1/getchildren";

    // 百度地图相关参数API
    // url
    public static final String BAIDU_MAP_AK = "";

    public static final String BAIDU_MAP_TO = "https://api.map.baidu.com/geocoding/v3/";

    // 阿里云上传相关参数
    public static final String endPoint = "oss-cn-beijing.aliyuncs.com";// 地域节点
    public static final String accessKeyId = "";
    public static final String accessKeySecret = "";
    public static final String bucketName = "xxxxx";// OSS的Bucket名称
    public static final String urlPrefix = "xxxxx.oss-cn-beijing.aliyuncs.com";// Bucket 域名
    public static final String fileHost = "cinema";// 目标文件夹
}
