package com.db.dbproject.config.shiro;


import org.apache.shiro.authc.pam.AtLeastOneSuccessfulStrategy;
import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

// shiro整个部分代码需要被强化！！！
@Configuration
public class ShiroConfig {
    // 配置管理工具
    @Bean
    public DefaultWebSecurityManager securityManager(WxRealm wxRealm,
                                                     AdminRealm adminRealm,
                                                     ModularRealmAuthenticator modularRealmAuthenticator,
                                                     DefaultWebSessionManager sessionManager){
        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        securityManager.setSessionManager(sessionManager);
        securityManager.setAuthenticator(modularRealmAuthenticator);
        // 加入需要验证的链式
        List<Realm> list = new ArrayList<Realm>();
        list.add(wxRealm);
        list.add(adminRealm);
        securityManager.setRealms(list);
        return securityManager;
    }
    // 管理员用户登录所需要的
    @Bean
    public AdminRealm adminRealm(){
        AdminRealm adminRealm = new AdminRealm();
        // 账号密码登录使用realm
        return adminRealm;
    }

    // 微信用户登录所需要的
    @Bean
    public WxRealm wxCodeRealm(){
        WxRealm wxRealm = new WxRealm();
        // 小程序使用openid登录使用的realm
        return wxRealm;
    }

    // 系统自带的Realm管理，主要针对多realm
    @Bean
    public ModularRealmAuthenticator modularRealmAuthenticator(){
        MyModularRealmAuthenticator modularRealmAuthenticator = new MyModularRealmAuthenticator();
        // 只要有一个成功就视为登录成功
        modularRealmAuthenticator.setAuthenticationStrategy(new AtLeastOneSuccessfulStrategy());
        return modularRealmAuthenticator;
    }

    // 管理对象的 DefaultWebSessionManager
    @Bean
    public DefaultWebSessionManager sessionManager(){
        // 重写DefaultWebSessionManager
        MiniSessionManager sessionManager = new MiniSessionManager();
        sessionManager.setGlobalSessionTimeout(3600000L);
        return sessionManager;
    }

    //开启注解支持 ???
    @Bean
    public DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator(){
        DefaultAdvisorAutoProxyCreator advisorAutoProxyCreator = new DefaultAdvisorAutoProxyCreator();
        advisorAutoProxyCreator.setProxyTargetClass(true);
        return advisorAutoProxyCreator;
    }
    // ????
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager){
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    // Shiro过滤器
    @Bean
    public ShiroFilterFactoryBean shiroFilter(SecurityManager securityManager){
        ShiroFilterFactoryBean bean = new ShiroFilterFactoryBean();
        bean.setSecurityManager(securityManager);
        // 配置登录的url和登录成功的url以及验证失败的url
        bean.setLoginUrl("/user/login");
        // 自定义过滤器
        LinkedHashMap<String, Filter> map = new LinkedHashMap<>();
        map.put("authc",new MyFormAuthenticationFilter());
        bean.setFilters(map);

        // 配置访问权限
        LinkedHashMap<String,String> filterChainDefinitionMap = new LinkedHashMap<>();

        // 访问权限控制一览表
        filterChainDefinitionMap.put("/user/isAuth","authc");
        filterChainDefinitionMap.put("/user/wxLogin", "anon");
        filterChainDefinitionMap.put("/admin/*","authc");
        filterChainDefinitionMap.put("/admin/user/login","anon");
        filterChainDefinitionMap.put("/ai","anon");

        filterChainDefinitionMap.put("/home/updateWish","authc");
        filterChainDefinitionMap.put("/home/getIsWish","authc");
        filterChainDefinitionMap.put("/Comment/getIsComment","authc");
        filterChainDefinitionMap.put("/Comment/addComment","authc");
        filterChainDefinitionMap.put("/Comment/upApprove","authc");
        filterChainDefinitionMap.put("/order/*","authc");
        // 加入过滤器
        bean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return bean;
    }

}
