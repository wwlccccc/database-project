package com.db.dbproject.config.exception;

import com.db.dbproject.config.util.Result;

import javax.naming.AuthenticationException;

public class AuthException extends AuthenticationException {
    public AuthException(String msg) {
        super(msg);
    }
}
