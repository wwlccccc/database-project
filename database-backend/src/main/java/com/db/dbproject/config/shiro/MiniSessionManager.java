package com.db.dbproject.config.shiro;


import org.apache.shiro.web.servlet.ShiroHttpServletRequest;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.apache.shiro.web.util.WebUtils;
import org.springframework.util.StringUtils;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.Serializable;

public class MiniSessionManager extends DefaultWebSessionManager {
    // 请求token
    public final static  String token_name="token";

    // 请求头
    public final static  String session_id_Source="request_header";

    // 判断getSessionId的来源之一
    @Override
    protected Serializable getSessionId(ServletRequest request, ServletResponse response){
        //这个修改就是从请求头里来获取
        String token = WebUtils.toHttp(request).getHeader(token_name);
        if(StringUtils.isEmpty(token)){
            //如果没有携带token参数则按照父类的方式在cookie进行获取
            return super.getSessionId(request,response);
        }else {
            //如果请求头中有 token 则其值为sessionId
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_SOURCE,session_id_Source);  // session_id的来源之一
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID,token);
            request.setAttribute(ShiroHttpServletRequest.REFERENCED_SESSION_ID_IS_VALID,Boolean.TRUE);
            return token;
        }

    }
}
