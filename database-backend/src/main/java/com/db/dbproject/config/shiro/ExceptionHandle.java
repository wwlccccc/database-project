package com.db.dbproject.config.shiro;

import com.db.dbproject.config.util.Result;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class ExceptionHandle {

    // shiro权限认证无权限
    @ResponseBody
    @ExceptionHandler(UnauthorizedException.class)
    public Result handleShiroException(Exception ex){
        return new Result(Result.ERROR,"无权限");
    }

    // shiro权限认证失败
    @ResponseBody
    @ExceptionHandler(AuthorizationException.class)
    public Result AuthorizationException(Exception ex){
        return new Result(Result.ERROR,"权限认证失败");
    }

    // 腾讯地图异常捕获
}
