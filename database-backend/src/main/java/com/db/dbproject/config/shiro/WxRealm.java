package com.db.dbproject.config.shiro;

import com.db.dbproject.entity.User;
import com.db.dbproject.mapper.UserMapper;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.realm.AuthenticatingRealm;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;

public class WxRealm extends AuthenticatingRealm {

    @Autowired
    private UserMapper userMapper;

    // 鉴权，openid判断用户是否绑定微信
    // 这部分时认真逻辑的实现
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        WxToken wxToken = (WxToken) token;
        String open_id = wxToken.getOpenId();
        //sysUserDao.getByWxOpenId 根据openid查询是否有已绑定的userid,有就时已绑定
        // 查询数据库啦
        User user = userMapper.selectByOpenId(open_id);
        if (user != null) {
            // 不为空 更新最后登录时间
            user.setLastLogin(new Date());
            userMapper.updateById(user);
        } else {
            user = wxToken.getUser();
            userMapper.insertUser(user);
        }

        // 返回验证信息
        AuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(user, "ok", this.getClass().getSimpleName());
        return authenticationInfo;
    }

    @Override
    public boolean supports(AuthenticationToken authenticationToken) {
        // 设置此Realm支持的Token
        return authenticationToken != null && (authenticationToken instanceof WxToken);
    }
}
