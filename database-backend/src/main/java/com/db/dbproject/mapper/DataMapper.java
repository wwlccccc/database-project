package com.db.dbproject.mapper;

import com.db.dbproject.entity.Vo.DataByCinemaAndMovie;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface DataMapper {

    @Select("select td.cinema_id,td.movie_id,sum(tor.price) as sum_price from t_days as td,t_times as tt,t_order as tor where td.id = tt.days_id and tt.id = tor.relate_id and tor.item_type = '电影票' group by td.cinema_id, td.movie_id;")
    List<DataByCinemaAndMovie> getDataByCinemaAndMovie();

    @Select("select td.cinema_id,sum(tor.price) as sum_price from t_days as td,t_times as tt,t_order as tor where td.id = tt.days_id and tt.id = tor.relate_id and tor.item_type = '电影票' group by td.cinema_id;")
    List<DataByCinemaAndMovie> getDataByCinema();

    @Select("select td.movie_id,sum(tor.price) as sum_price from t_days as td,t_times as tt,t_order as tor where td.id = tt.days_id and tt.id = tor.relate_id and tor.item_type = '电影票' group by td.movie_id;")
    List<DataByCinemaAndMovie> getDataByMovie();
}
