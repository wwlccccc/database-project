package com.db.dbproject.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.db.dbproject.entity.Movie;
import com.db.dbproject.entity.MovieWish;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MovieMapper extends BaseMapper<Movie> {

    @Select("select * from t_movie where showst=3 and id in (select movie_id from t_days where cinema_id in (select id from t_cinema where addr like '%${addr}%')) order by watched desc,sc desc")
    List<Movie> getList(String addr);

    @Select("select * from t_movie where showst!=3 order by wish desc,rt asc")
    List<Movie> getWait();

    @Select("select * from t_movie where showst!=3 order by wish desc limit 0,8")
    List<Movie> getMoviePop();

    // 这里是全文搜索，建议使用sql全文搜索实现
    @Select("select * from t_movie where match(nm,cat,dir,dra,star) against ('%${keyword}%')order by nm asc")
    List<Movie> findByNmLikeOrDirLikeOrCatLikeOrDraLikeOrStarContains(String keyword);

    @Select("select * from t_movie where nm=#{nm}")
    Movie getMovieByName(String nm);

    @Select("<script>" +
            "select * from t_movie where 1=1" +
            "<if test='keyword != null'>" +
            "and (nm like '%${keyword}%' or enm like '%${keyword}%' " +
            "or cat like '%${keyword}%' or dir like '%${keyword}%' or dra like '%${keyword}%')" +
            "</if>" +
            "</script>")
    List<Movie> getMovies(String keyword);

    @Select("select * from t_movie where showst=3")
    List<Movie> getAllMovie();

    @Select("<script>" +
            "select * from t_movie where id in " +
            "(select movie_id from t_days " +
            "where cinema_id=#{cinemaId} and to_days(day)>=to_days(now()))" +
            "</script>")
    List<Movie> getMovieByCinemaIdAndDay(Integer cinemaId);

    @Select("select * from t_movie where id=#{id}")
    Movie getMovieById(Integer id);


    @Update("update t_movie set wish = wish+#{num} where id=#{movieId}")
    void upWish(Integer movieId,Integer num);

    @Select("select * from t_movie where id!=#{movieId} and showst!=1 and cat like '%${cat}%' limit #{limit}")
    List<Movie> getLikeMovieList(Integer movieId, String cat,Integer limit);
}
