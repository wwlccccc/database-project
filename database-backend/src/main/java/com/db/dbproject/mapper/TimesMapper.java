package com.db.dbproject.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.db.dbproject.entity.Cinema;
import com.db.dbproject.entity.Times;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface TimesMapper extends BaseMapper<Times> {
    @Select("<script>" +
            "select * from t_times where days_id in" +
            "(select id from t_days where movie_id in " +
            "(select id from t_movie where nm like '%${keyword}%') " +
            "<if test='cinemaId!=null'>" +
            "and cinema_id=#{cinemaId} " +
            "</if>)" +
            "order by start_time desc" +
            "</script>")
    // 关键词茶味为电影名
    List<Times> getAll(String keyword, Integer cinemaId);

    // 查询放映时间表
    @Select("select * from t_times where days_id=#{dasId} and to_days(start_time)=to_days(#{day}) and start_time>now() order by start_time")
    List<Times> getByDaysId(Integer dasId, Date day);

    @Select("<script>" +
            "select * from t_cinema where id in " +
            "(select cinema_id from t_hall where id in (" +
            "select hall_id from t_times where id=#{relateId}))" +
            "</script>")
    Cinema getCinemaId(Integer relateId);

}
