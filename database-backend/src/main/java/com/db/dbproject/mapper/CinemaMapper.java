package com.db.dbproject.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.db.dbproject.entity.Cinema;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public interface CinemaMapper extends BaseMapper<Cinema> {
    @Select("SELECT *,round((6378.138 * 2 * ASIN(SQRT(POW(SIN((#{latitude1} * PI() / 180 - t1.latitude * PI() / 180) / 2),2) + COS(#{latitude2} * PI() / 180) * COS(t1.latitude * PI() / 180) * POW(SIN((#{longitude} * PI() / 180 - t1.longitude * PI() / 180) / 2),2))) * 1000))/1000 AS distance from t_cinema as t1 where match(nm,addr) against ('%${keyword}%') order by distance asc ;")
    List<Cinema> getCinemasByKeyWordAndDIstance(double latitude1, double latitude2, double longitude, String keyword);

    @Select("select * from t_cinema where nm like '%${keyword}%' or addr like '%${keyword}%'")
    List<Cinema> getCinema(String keyword);

    @Select("select * from t_cinema")
    List<Cinema> selectAll();

    @Select("<script>" +
            "SELECT *,(round(6367000 * 2 * asin(sqrt(pow(sin(((latitude * pi()) / 180 - (#{lat} * pi()) / 180) / 2), 2) " +
            "+ cos((#{lat} * pi()) / 180) * cos((latitude * pi()) / 180) * pow(sin(((longitude * pi()) / 180 " +
            "- (#{lng} * pi()) / 180) / 2), 2))))) AS distance " +
            "FROM `t_cinema` where addr like '%${adrr}%' " +
            "<if test='selectRegion != null'>"+
            "and addr like '%${selectRegion}%' " +
            "</if>"+
            "<if test='brandId != -1'>"+
            "and brand_id=#{brandId} " +
            "</if>"+
            "<if test='serviceId == 1'>"+
            "and endorse=true " +
            "</if>"+
            "<if test='serviceId == 2'>"+
            "and allowrefund=true " +
            "</if>"+
            "<if test='hallType != -1'>"+
            "and id in " +
            "(select cinema_id from t_hall where hall_type_id=#{hallType}) "+
            "</if>"+
            "and id in (select cinema_id from t_days where 1=1 " +
            "<if test='movieId != null'>"+
            "and movie_id=#{movieId} " +
            "</if>"+
            "<if test='date != null'>"+
            "and day=#{date}" +
            "</if>"+
            ") ORDER BY distance asc" +
            "</script>")
    // 筛选电影院
    List<Cinema> getCinemas(Integer movieId,String date,BigDecimal lat,BigDecimal lng, String adrr,Integer brandId,Integer serviceId,Integer hallType,String selectRegion);

}
