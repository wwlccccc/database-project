package com.db.dbproject.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.db.dbproject.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper extends BaseMapper<User> {
    // 根据open_id选择用户
    @Select("select * from t_user where open_id = #{open_id}")
    User selectByOpenId(String open_id);

    // 新增用户
    @Insert("insert into t_user (avatar_url,nick_name,gender,open_id,last_login) value(#{avatarUrl},#{nickName},#{gender},#{openId},#{lastLogin})")
    void insertUser(User insert_user);

    // 查询所有用户
    @Select("select * from t_user where nick_name like '%${keyword}%' or gender like '%${keyword}%'")
    List<User> getUsers(String keyword);
}
