package com.db.dbproject.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.db.dbproject.entity.MovieWish;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieWishMapper extends BaseMapper<MovieWish> {
    @Select("select * from t_movie_wish where user_id=#{userId} and movie_id=#{movieId}")
    MovieWish getByUserAndMovie(Integer userId, Integer movieId);

    @Insert("insert into t_movie_wish (user_id,movie_id) values (#{userId},#{movieId})")
    void addWish(Integer userId, Integer movieId);
}
