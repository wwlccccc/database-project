import json
import json
import requests
from requests.exceptions import RequestException
import re
import time
import csv
import xlwt
from bs4 import BeautifulSoup
import random

name = "hotmovie"


def getHotMovie():
    for i in range(0, 1):
        time.sleep(2.5)  # 延迟访问 但感觉对猫眼电影这个网站来说没用！！

        url = "https://m.maoyan.com/ajax/movieOnInfoList"
        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Mobile Safari/537.36'
        }
        try:
            response = requests.get(url, headers=headers)
            if response.status_code == 200:
                # print(response.text)  # 这里需要返回页面源码 以便于解析
                with open(str(name) + ".json", 'w', encoding="utf-8") as f:
                    # print(type(response.json()))
                    json.dump(response.json(), f, ensure_ascii=False)
                return
            return None
        except RequestException:
            return None


def getMovieDetail(movieName,movieId):
    for i in range(0, 1):
        time.sleep(2.5)  # 延迟访问 但感觉对猫眼电影这个网站来说没用！！

        url = "https://m.maoyan.com/ajax/detailmovie?movieId="+str(movieId)
        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Mobile Safari/537.36'
        }
        try:
            response = requests.get(url, headers=headers)
            if response.status_code == 200:
                # print(response.text)  # 这里需要返回页面源码 以便于解析
                with open(str(movieName) + ".json", 'w', encoding="utf-8") as f:
                    # print(type(response.json()))
                    json.dump(response.json(), f, ensure_ascii=False)
                return
            return None
        except RequestException:
            return None


if __name__ == "__main__":
    # getHotMovie()
    with open(name + ".json", encoding="utf-8") as file:
        movie = json.load(file)

    movieList = []
    for item in movie.keys():
        if item == "movieList":
            movieList = movie["movieList"]

    for i in range(0, len(movieList)):
        print(movieList[i])
        movieId = movieList[i]['id']
        getMovieDetail(movieList[i]['nm'],movieId)
        # 构造电影详情查询请求

    print(len(movieList))
