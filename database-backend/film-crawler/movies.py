import json
import os
# 导入pymysql包
from datetime import datetime

import pymysql


# 特定类型字符串拼接
def stringstr(urls):
    if (len(urls) == 0):
        return ''
    for j in range(0, len(urls) - 1):
        str = urls[j] + ","
    str = "" + str + urls[len(urls) - 1]
    return str


def updateData(indexList):
    # 创建数据库连接
    conn = pymysql.connect(host="localhost", port=3306, user="root", passwd="123456", db="movie")
    # 获取一个游标对象
    cursor = conn.cursor()
    json_text = r'.\movies.json'
    with open(json_text, encoding='utf8') as f:
        content = json.load(f)
        for i in range(0, len(indexList)):
            print("长度", len(content[i]["photos"]))
            str = stringstr(content[i]["photos"])
            # print(dict)
            # sql语句中，用%s做占位符，参数用一个元组
            # nm = content[i]["nm"] ? content[i]["nm"] : None
            sql = "insert into t_movie(nm, enm, cat, dir, img, version, dra, sc, snum, star, src, language, dur,globalReleased, wish, watched, rt, showst, video_img, video_name, video_url, photos) " \
                  "values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            # 这个异常出处理不是很好，得修改
            param = (content[i]["nm"], content[i]["enm"], content[i]["cat"], content[i]["dir"],
                     content[i]["img"], content[i]["ver"], content[i]["dra"], content[i]["sc"],
                     content[i]["snum"], content[i]["star"], content[i]["src"],
                     content[i]["oriLang"], content[i]["dur"], content[i]["globalReleased"],
                     content[i]["wish"], content[i]["watched"], content[i].setdefault("rt", '2023-01-01'),
                     content[i]["showst"],
                     content[i].setdefault("videoImg", ""),
                     content[i].setdefault("videoName", ""), content[i].setdefault("videourl", ""), str)
            # 执行数据库插入
            cursor.execute(sql, param)
            # 提交
            conn.commit()
    # 关闭连接
    conn.close()
    cursor.close()


# 读取json文件写入数据
def writeJson2db(folder_path, conn, cursor):
    for item in os.listdir((folder_path)):
        json_path = os.path.join(folder_path, item)
        with open(json_path, encoding="utf-8") as f:
            content = json.load(f)
            print(len(content))
            print((content.keys()))
            assert len(content) == 1
            # 调入写入函数，写入db数据库文件
            writeTodb(content["detailMovie"], conn, cursor)


def writeTodb(content, conn, cursor):
    str = stringstr(content["photos"])
    # sql语句中，用%s做占位符，参数用一个元组
    sql = "insert into t_movie(nm, enm, cat, dir, img, version, dra, sc, snum, star, src, language, dur,globalReleased, wish, watched, rt, showst, video_img, video_name, video_url, photos) " \
          "values (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
    # 这个异常出处理不是很好，得修改
    param = (content["nm"], content["enm"], content["cat"], content["dir"],
             content["img"], content["ver"], content["dra"], content["sc"],
             content["snum"], content["star"], content["src"],
             content["oriLang"], content["dur"], content["globalReleased"],
             content["wish"], content["watched"], content.setdefault("rt", '2023-01-01'),
             content["showst"],
             content.setdefault("videoImg", ""),
             content.setdefault("videoName", ""), content.setdefault("videourl", ""), str)
    # 执行数据库插入
    cursor.execute(sql, param)
    # 提交
    conn.commit()


if __name__ == '__main__':
    # 创建数据库连接
    conn = pymysql.connect(host="localhost", port=3306, user="root", passwd="123456", db="weipiao")
    # 获取一个游标对象
    cursor = conn.cursor()
    writeJson2db("./data/", conn, cursor)
    # 关闭连接
    conn.close()
    cursor.close()
#     list1 = getIndex()
#     updateData(list1)
#     # print(getIndex())
#     print(len(list1))
#     for r in list1:
#         print(r)
