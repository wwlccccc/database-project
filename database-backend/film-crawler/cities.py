import json
import json
import requests
from requests.exceptions import RequestException
import re
import time
import csv
import xlwt
from bs4 import BeautifulSoup
import random


def getCity():
    # 解析猫眼电影的json数据
    with open("cities.json", encoding="utf-8") as city:
        cities = json.load(city)

    letterMap = cities['letterMap']
    # print(letterMap)
    # print(letterMap.keys())

    # 将城市数据存入数组中
    city_id = []
    for item in letterMap.keys():
        city_item = letterMap[item]
        # print(len(city_item))
        for i in range(len(city_item)):
            # print(city_item[i])
            city_id.append(city_item[i])
    # print(city_id)
    # print(len(city_id))
    # print(type(cities))
    # print(cities.keys())
    # 获取城市id和城市名称
    # print(city_id.__getitem__(25)['id'])
    # print(city_id.__getitem__(25)['nm'])


if __name__ == "__main__":
    getCity()


# 构造get请求获取城市影院信息
def getCityCinema(cityid):
    for i in range(0, 1):
        time.sleep(2.5)  # 延迟访问 但感觉对猫眼电影这个网站来说没用！！
        url = "https://m.maoyan.com/ajax/filterCinemas?ci=" + str(cityid)
        headers = {
            'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Mobile Safari/537.36'
        }
        try:
            response = requests.get(url, headers=headers)
            if response.status_code == 200:
                # print(response.text)  # 这里需要返回页面源码 以便于解析
                with open(str(cityid) + ".json", 'w', encoding="utf-8") as f:
                    # print(type(response.json()))
                    json.dump(response.json(), f, ensure_ascii=False)
                return
            return None
        except RequestException:
            return None


if __name__ == '__main__':
    # getCityCinema(96)
    with open("cities/96.json", encoding="utf-8") as city:
        cinemas = json.load(city)
    print(cinemas.keys())
    # print(cinemas)
    # dict_keys(['brand', 'district', 'hallType', 'service', 'showType', 'subway', 'timeRanges'])

    # 获取brandlist列表
    brandlist = []
    brand = cinemas["brand"]["subItems"]
    for i in range(0, len(brand)):
        brandlist.append(brand[i])

    print(brandlist)

    # 获取行政区域列表

    districtList = []
    district = cinemas["district"]["subItems"]
    for i in range(0, len(district)):
        districtList.append(district[i])

    print(districtList)

    # print(brand)
    # for item in cinemas.keys():
    #     keyword = cinemas[item]
    #     print(len(keyword))
    #     for i in range(0,len(keyword)):
    #         print(keyword[i])

if __name__ == '__main__':
    getCityCinema(96)
