import json
import requests
from requests.exceptions import RequestException
import re
import time
import csv
import xlwt
from bs4 import BeautifulSoup
import random

data_list = []  # 定义一个全局列表


def get_one_page(url):  # 请求页面函数
    headers = {
        'User-Agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Mobile Safari/537.36'
    }

    try:
        response = requests.get(url, headers=headers)
        if response.status_code == 200:
            # print(response.text)#这里需要返回页面源码 以便于解析
            return response
        return None
    except RequestException:
        return None


def parse_one_page(html):  # 解析页面函数

    soup = BeautifulSoup(html, 'lxml')
    items = soup.select('dd')
    # print(items)
    for item in items:
        name = item.select('p.name a')[0].string
        actor = item.select('p.star')[0].string.strip()
        time = item.select('p.releasetime')[0].string
        fraction = item.select('p.score>i')[0].string
        fraction2 = item.select('p.score i.fraction')[0].string
        # 这里其实有一点我没处理好就是评分这段 页面显示的是“9.6” 我select用的不够好 不能把数据一起爬下来 只能分段爬取 “9.”和“6” 这个小bug后面弄好了 我会更新的
        data_dict = {}

        data_dict['名字'] = name
        data_dict['演员'] = actor
        data_dict['上映时间'] = time
        data_dict['总评分'] = fraction
        data_dict['小数'] = fraction2

        data_list.append(data_dict)

        print(data_list)


def main():
    for i in range(0, 1):
        time.sleep(2.5)  # 延迟访问 但感觉对猫眼电影这个网站来说没用！！
        url = 'https://maoyan.com/board/4?offset=' + str(i * 10)
        html = get_one_page(url)
        print(html)
        parse_one_page(html)  # 解析的页面

    # for循环全部结束以后在写入文件
    with open('data_csv.csv', 'w', encoding='utf-8', newline='') as f:  # 创建了个文件 作为f

        title = data_list[0].keys()  # 表头

        # 创建writer对象
        writer = csv.DictWriter(f, title)  # 写入表头

        writer.writeheader()  # 写入表头
        # 批量写入数据
        writer.writerows(data_list)

    print('csv文件写入完成')


if __name__ == '__main__':
    main()
