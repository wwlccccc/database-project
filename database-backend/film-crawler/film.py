import requests
from fake_useragent import UserAgent
import csv
import json

import urllib3

urllib3.disable_warnings()


# fp = open('美团信息.csv', 'a', newline='', encoding='utf-8-sig')
# writer = csv.writer(fp)
# writer.writerow(('用户ID', '用户名', '平均价', '评论', '回复'))  # 表头

# for page in range(0, 515, 10):  # 步长为10迭代
#     url = "https://www.meituan.com/meishi/api/poi/getMerchantComment?uuid=2ff7056c-9d76-424c-b564-b7084f7e16e4&platform=1&" \
#           "partner=126&originUrl=https%3A%2F%2Fwww.meituan.com%2Fmeishi%2F193383554%2F&riskLevel=1&optimusCode=10&id=193383554&userId=&offset={}&pageSize=10&sortType=1".format(
#         page)
#     # 构建headers
#     headers = {
#         'User_Agent': UserAgent().chrome
#     }
#     respone = requests.get(url, headers=headers)
#     open('html.txt', 'w', encoding='utf-8').write(respone.text)
#     print(respone.content.decode('utf-8'))
#     comments = json.loads(respone.content.decode('utf-8'))['data']['comments']
#     for item in comments:
#         userId = item['userId']  # 评论者id
#         userName = item['userName']  # 评论者昵称
#         avgPrice = item['avgPrice']  # 平均价格
#         comment = item['comment']  # 评论内容
#         merchantComment = item['merchantComment']  # 商家回复
#         data = (userId, userName, avgPrice, comment, merchantComment)
#         writer.writerow(data)
# 爬取热映的
# url = request_data
# headers = {
#     'User_Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
# }
# respone = requests.get(url, headers=headers, stream=True)
# requests.packages.urllib3.disable_warnings()
# print(respone.content.decode('utf-8'))
# respone.close()
# idList = ''
# for id in idList:
#     url = "https://m.maoyan.com/ajax/detailmovie?movieId={}".format(id


def request_data(url):
    headers = {
        'User_Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.102 Safari/537.36'
    }
    req = requests.get(url, timeout=30, headers=headers, stream=True)  # 请求连接
    print(req.content)
    req.packages.urllib3.disable_warnings()
    req_jason = req.json()  # 获取数据
    return req_jason


if __name__ == '__main__':
    print(request_data("https://m.maoyan.com/ajax/movieOnInfoList"))
