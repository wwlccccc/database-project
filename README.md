# database-project

#### Introduction

本项目是2022年秋季sdu-sc-database-project. 项目主题：电影院售票管理系统。

本项目分为用户端和管理员后台两个部分：

- 用户端：微信小程序

- 管理员后台：Vue、ElementUI、Shiro、Mysql、Redis、SpringBoot

#### 环境依赖

| 环境依赖   | 说明描述               |
| ---------- | ---------------------- |
| Java       | JDK1.8及以上           |
| Vue        | 需要支持Vue2的node版本 |
| Mysql      | Mysql8                 |
| SpringBoot | 2.2.4.RELEASE          |

安装说明请参考[Install.md](./Install.md)

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
