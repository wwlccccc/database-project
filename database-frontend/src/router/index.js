import Vue from 'vue'
import Router from 'vue-router'
import Login from "../pages/Login/Login";
import Home from "../pages/Home/Home";
import UserManage from "../pages/Home/children/UserManage";
import SnackManage from "../pages/Home/children/SnackManage";
import OrderManage from "../pages/Home/children/OrderManage";
import MovieManage from "../pages/Home/children/MovieManage";
import MovieSchedule from "../pages/Home/children/MovieSchedule";
import HallManage from "../pages/Home/children/HallManage";
import CommentManage from "../pages/Home/children/CommentManage";
import CinemaManage from "../pages/Home/children/CinemaManage";
import BannerManage from "../pages/Home/children/BannerManage";
import AdminManage from "../pages/Home/children/AdminManage";
import UploadFiles from "../pages/Home/children/UploadFiles";
import Echarts from "../pages/Home/children/Echarts";

import Business from "../pages/Business/Business";
import BHallManage from "../pages/Business/children/HallManage";
import BMovieSchedule from "../pages/Business/children/MovieSchedule";
import BOrderManage from "../pages/Business/children/OrderManage";
import BSnackManage from "../pages/Business/children/SnackManage";
Vue.use(Router)


export default new Router({
  routes: [
    {path:'/',redirect:'/login',},
    {
      path:'/login',
      name:'login',
      component:Login
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      children:[
        {path: 'echarts',component: Echarts},
        {path:'user_manage',component:UserManage},
        {path:'movie_manage',component:MovieManage},
        {path:'cinema_manage',component:CinemaManage},
        {path:'hall_manage',component:HallManage},
        {path:'snack_manage',component:SnackManage},
        {path:'banner_manage',component:BannerManage},
        {path:'admin_manage',component:AdminManage},
        {path:'comment_manage',component:CommentManage},
        {path:'order_manage',component:OrderManage},
        {path:'movie_schedule',component:MovieSchedule},
        {path:'/home',redirect: '/home/user_manage'},
        {path: 'uploadFiles',component: UploadFiles}
      ]
    },
    {
      path: '/business',
      name: 'business',
      component: Business,
      children:[
        {path:'border_manage',component:BOrderManage},
        {path:'bhall_manage',component:BHallManage},
        {path:'bmovie_schedule',component:BMovieSchedule},
        {path:'bsnack_manage',component:BSnackManage},
        {path:'/business',redirect: '/business/border_manage'},
      ]
    }
  ]
})
